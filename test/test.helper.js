/* eslint-disable no-undef */
const mongoose = require("mongoose");
require("dotenv").config();
const logger = require("../src/config/config").logger;

before(done => {
	require("tracer").setLevel(process.env.LOG_LEVEL || "warn");

	if (process.env.MONGODB_TEST_CONNECTION_URL) {
		mongoose.connect(
			process.env.MONGODB_TEST_CONNECTION_URL,
			{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }
		);
	
		mongoose.connection
			.once("open", () => done())
			.on("error", err => {
				logger.error("Warning", err);
			});
	} else 
		throw Error("Error: No mongoDB connection URL defined in the environment variable 'MONGODB_TEST_CONNECTION_URL'");
});

beforeEach(done => {
	// eslint-disable-next-line no-unused-vars
	mongoose.connection.db.dropDatabase((error, _) => {
		if (error) logger.error(error);
		done();
	});
});

after(() => mongoose.connection.dropDatabase());
