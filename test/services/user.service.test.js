/* eslint-disable no-undef */
const chai = require("chai");
const expect = chai.expect;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
require("dotenv").config();
const config = require("../../src/config/config");

const User = require("../../src/models/user.model");
const UserService = require("../../src/services/user.service");

describe("In testing the user service, ", function() {
	this.retries(1);

	describe("For the Administrator routes", function() {

		this.beforeEach(done => {
			bcrypt.hash("T3stP@ssw0rd!", config.security.bcrypt.saltRounds).then(hashedPassword =>
				User.create({
					emailAddress: "one.admin@tripleProblematiek.nl",
					firstName: "Test",
					lastName: "Admin",
					function: "Test Function",
					password: hashedPassword,
					administrator: true
				})).then(() =>
				done());
		});
		describe("Using the registerAdministrator service, ", () => {
			it("Registers a new administrator", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						emailAddress: "other.admin@tripleProblematiek.nl",
						firstName: "Test Other",
						lastName: "Admin",
						phoneNumber: "+31-655-5152-20",
						function: "Test Function",
						password: "T3stP@ssw0rd!"
					}).then(administrator => {
						expect(administrator).to.haveOwnProperty("token");
						expect(administrator).to.haveOwnProperty("firstName", "Test Other");
						expect(administrator).to.haveOwnProperty("lastName", "Admin");
						const tokenPayload = jwt.verify(administrator.token, config.security.jwt.accessToken);
						expect(tokenPayload).to.haveOwnProperty("emailAddress", "other.admin@tripleProblematiek.nl");
						expect(tokenPayload).to.haveOwnProperty("administrator", true);
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount + 1);
							User.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount + 1);
								done();
							});
						});
					});
				});
			});

			it("Fails if required value is empty or just contains whitespace characters", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						emailAddress: "", // Empty String
						firstName: "            \n", // Only whitespace characters
						lastName: "User",
						function: "Test Function",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: emailAddress: Email address is required!, firstName: First name is required!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if firstname is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						emailAddress: "other.admin@tripleProblematiek.nl",
						function: "Test Function",
						lastName: "User",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: firstName: First name is required!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if lastname is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						emailAddress: "other.admin@tripleProblematiek.nl",
						function: "Test Function",
						firstName: "Test",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: lastName: Last name is required!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if email address is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						function: "Test Function",
						firstName: "Test",
						lastName: "User",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: emailAddress: Email address is required!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if function is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						emailAddress: "other.admin@tripleProblematiek.nl",
						firstName: "Test",
						lastName: "User",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: function: Function is required!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if password is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						firstName: "Test",
						lastName: "User",
						emailAddress: "other.admin@tripleProblematiek.nl"
					}).catch(error => {
						expect(error).to.equal("This password does not conform to the requirements!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if password is invalid", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						firstName: "Test",
						lastName: "User",
						emailAddress: "other.admin@tripleProblematiek.nl",
						function: "Test Function",
						password: "InvalidPassword!"
					}).catch(error => {
						expect(error).to.equal("This password does not conform to the requirements!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});
	
			it("Fails if email address already exists", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						firstName: "Test",
						lastName: "User",
						emailAddress: "one.admin@tripleProblematiek.nl",
						function: "Test Function",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: emailAddress: This Email address already has an account!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if phone number is invalid", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						firstName: "Test",
						lastName: "User",
						emailAddress: "other.admin@tripleProblematiek.nl",
						phoneNumber: "Invalid Phone Number",
						function: "Test Function",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: phoneNumber: Phone number must be valid!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});
		});
	
		describe("Using the loginAdministrator service, ", () => {
			it("Logs in an administrator", done => {
				UserService.loginUser({
					emailAddress: "one.admin@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				}).then(administrator => {
					expect(administrator).to.haveOwnProperty("token");
					expect(administrator).to.haveOwnProperty("firstName", "Test");
					expect(administrator).to.haveOwnProperty("lastName", "Admin");
					const tokenPayload = jwt.verify(administrator.token, config.security.jwt.accessToken);
					expect(tokenPayload).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
					expect(tokenPayload).to.haveOwnProperty("administrator", true);
					done();
				});
			});
	
			it("Fails if email address is missing", done => {
				UserService.loginUser({
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				}).catch(error => {
					expect(error).to.haveOwnProperty("message", "Cannot read property 'password' of null");
					done();
				});
			});
	
			it("Fails if password is missing", done => {
				UserService.loginUser({
					emailAddress: "other.admin@tripleProblematiek.nl"
				}).catch(error => {
					expect(error).to.haveOwnProperty("message", "Cannot read property 'password' of null");
					done();
				});
			});
	
			it("Fails if email address doesn't exist", done => {
				UserService.loginUser({
					emailAddress: "other.admin@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				}).catch(error => {
					expect(error).to.haveOwnProperty("message", "Cannot read property 'password' of null");
					done();
				});
			});
	
			it("Fails if password is wrong", done => {
				UserService.loginUser({
					emailAddress: "one.admin@tripleProblematiek.nl",
					function: "Test Function",
					password: "Wrong Password"
				}).catch(error => {
					expect(error).to.equal("Incorrect Password!");
					done();
				});
			});
		});
	});

	describe("For the User routes", function() {

		this.beforeEach(done => {
			bcrypt.hash("T3stP@ssw0rd!", config.security.bcrypt.saltRounds).then(hashedPassword =>
				User.create({
					emailAddress: "one.user@tripleProblematiek.nl",
					firstName: "Test",
					lastName: "User",
					function: "Test Function",
					password: hashedPassword,
					administrator: false
				})).then(() =>
				done());
		});

		describe("Using the registerUser service, ", () => {
			it("Registers a new user", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerUser({
						firstName: "Test",
						lastName: "User",
						emailAddress: "other.user@tripleProblematiek.nl",
						phoneNumber: "+31-655-5152-20",
						function: "Test Function",
						password: "T3stP@ssw0rd!"
					}).then(user => {
						expect(user).to.haveOwnProperty("token");
						expect(user).to.haveOwnProperty("firstName", "Test");
						expect(user).to.haveOwnProperty("lastName", "User");
						const tokenPayload = jwt.verify(user.token, config.security.jwt.accessToken);
						expect(tokenPayload).to.haveOwnProperty("emailAddress", "other.user@tripleProblematiek.nl");
						expect(tokenPayload).to.haveOwnProperty("administrator", false);
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount + 1);
							done();
						});
					});
				});
			});

			it("Fails if firstname is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						emailAddress: "other.user@tripleProblematiek.nl",
						function: "Test Function",
						lastName: "User",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: firstName: First name is required!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if lastname is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						emailAddress: "other.user@tripleProblematiek.nl",
						function: "Test Function",
						firstName: "Test",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: lastName: Last name is required!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});
	
			it("Fails if email address is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerUser({
						firstName: "Test",
						lastName: "User",
						function: "Test Function",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: emailAddress: Email address is required!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if function is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerAdministrator({
						firstName: "Test",
						lastName: "User",
						emailAddress: "other.user@tripleProblematiek.nl",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: function: Function is required!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});
	
			it("Fails if password is missing", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerUser({
						firstName: "Test",
						lastName: "User",
						emailAddress: "other.user@tripleProblematiek.nl"
					}).catch(error => {
						expect(error).to.equal("This password does not conform to the requirements!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if password is invalid", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerUser({
						firstName: "Test",
						lastName: "User",
						emailAddress: "other.user@tripleProblematiek.nl",
						function: "Test Function",
						password: "InvalidPassword"
					}).catch(error => {
						expect(error).to.equal("This password does not conform to the requirements!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});
	
			it("Fails if email address already exists", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerUser({
						firstName: "Test",
						lastName: "User",
						emailAddress: "one.user@tripleProblematiek.nl",
						function: "Test Function",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: emailAddress: This Email address already has an account!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});

			it("Fails if phone number is invalid", done => {
				User.countDocuments().then(oldCount => {
					UserService.registerUser({
						firstName: "Test",
						lastName: "User",
						emailAddress: "other.user@tripleProblematiek.nl",
						phoneNumber: "Invalid Phone Number",
						function: "Test Function",
						password: "T3stP@ssw0rd!"
					}).catch(error => {
						expect(error).to.haveOwnProperty("message", "user validation failed: phoneNumber: Phone number must be valid!");
						User.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});
		});
	
		describe("Using the loginUser service, ", () => {
			it("Logs in a user", done => {
				UserService.loginUser({
					emailAddress: "one.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				}).then(user => {
					expect(user).to.haveOwnProperty("token");
					expect(user).to.haveOwnProperty("firstName", "Test");
					expect(user).to.haveOwnProperty("lastName", "User");
					const tokenPayload = jwt.verify(user.token, config.security.jwt.accessToken);
					expect(tokenPayload).to.haveOwnProperty("emailAddress", "one.user@tripleProblematiek.nl");
					expect(tokenPayload).to.haveOwnProperty("administrator", false);
					done();
				});
			});
	
			it("Fails if email address is missing", done => {
				UserService.loginUser({
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				}).catch(error => {
					expect(error).to.haveOwnProperty("message", "Cannot read property 'password' of null");
					done();
				});
			});
	
			it("Fails if password is missing", done => {
				UserService.loginUser({
					emailAddress: "other.user@tripleProblematiek.nl"
				}).catch(error => {
					expect(error).to.haveOwnProperty("message", "Cannot read property 'password' of null");
					done();
				});
			});
	
			it("Fails if email address doesn't exist", done => {
				UserService.loginUser({
					emailAddress: "other.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				}).catch(error => {
					expect(error).to.haveOwnProperty("message", "Cannot read property 'password' of null");
					done();
				});
			});
	
			it("Fails if password is wrong", done => {
				UserService.loginUser({
					emailAddress: "one.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "Wrong Password"
				}).catch(error => {
					expect(error).to.equal("Incorrect Password!");
					done();
				});
			});
		});
	});
});
