/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const chai = require("chai");
const expect = chai.expect;
const ObjectId = require("mongoose").Types.ObjectId;

// Models
const Article = require("../../src/models/article.model");
const User = require("../../src/models/user.model");

// Services
const UserService = require("../../src/services/user.service");
const ArticleService = require("../../src/services/article.service");
const { logger } = require("../../src/config/config");

describe("In testing the article service, ", function() {
	this.retries(1);

	describe("For the Article routes run by an Administrator", () => {
		let articleId;

		beforeEach(done => {
			UserService.registerAdministrator({
				firstName: "Test",
				lastName: "Admin",
				emailAddress: "one.admin@tripleProblematiek.nl",
				function: "Test Function",
				password: "T3stP@ssw0rd!"
			}).then(({ newToken, firstName, lastName }) => {
				token = newToken;
				return UserService.registerUser({
					firstName: "Test",
					lastName: "User",
					emailAddress: "one.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				});
			}).then(() => done());
		});

		describe("Using the createArticle service, ", () => {
			it("Passes if article is successfully created", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								title: "Test Title",
								body: "This is a test body",
								category: "signaleren"
							}, { id: admin._id }).then(articleDocument => {
								const article = articleDocument._doc;
								expect(article).to.haveOwnProperty("title", "Test Title");
								expect(article).to.haveOwnProperty("body", "This is a test body");
								expect(article).to.haveOwnProperty("category", "signaleren");
								expect(article).to.haveOwnProperty("owner", admin._id);
								expect(article).to.haveOwnProperty("date");
								expect(article).to.haveOwnProperty("verified", true);
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
						});
				});
			});

			it("Fails if required value is empty or just contains whitespace characters", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								title: "     			\n",
								body: "",
								category: "signaleren"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: title: Title is required!, body: Body is required!");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if title is missing", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								body: "This is a test body",
								category: "signaleren"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: title: Title is required!");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if body is missing", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								title: "Test Title",
								category: "signaleren"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: body: Body is required!");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if category is missing", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								title: "Test Title",
								body: "This is a test body"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: category: Category is required!");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if category is not a correct enum", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								title: "Test Title",
								body: "This is a test body",
								category: "Wrong Category"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: category: `Wrong Category` is not a valid enum value for path `category`.");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if the creating user doesn't exist", done => {
				Article.countDocuments().then(oldCount => {
					ArticleService.createArticle({
						title: "Test Title",
						body: "This is a test body",
						category: "Wrong Category"
					}, { id: new ObjectId() }).catch( error => {
						expect(error).to.equal("This user does not exist!");
						Article.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});
		});

		describe("Using the updateArticle service", () => {
			beforeEach(done => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(user => ArticleService.createArticle({
						title: "new Article title", 
						body: "New Lorem Ipsum article body",
						category: "signaleren",
					}, { id: user._id }))
					.then(article => {
						articleId = article._id;
						done();
					});
			});

			it("Passes if the article got successfully updated", done => {
				const articleProps = {
					title: "Brand new Article title",
					body: "Ipsum article body",
					category: "diagnostiek"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.then(articleDocument => {
									const article = articleDocument._doc;
									expect(article).to.haveOwnProperty("_id");
									expect(article._id.toString()).to.equal(articleId.toString());
									expect(article).to.haveOwnProperty("title", "Brand new Article title");
									expect(article).to.haveOwnProperty("body", "Ipsum article body");
									expect(article).to.haveOwnProperty("category", "diagnostiek");
									expect(article).to.haveOwnProperty("owner");
									expect(article.owner.toString()).to.equal(user._id.toString());
									expect(article).to.haveOwnProperty("date");
									expect(article.date.toJSON()).to.equal(oldArticle.date);
									expect(article).to.haveOwnProperty("verified", true);
									done();
								}));
					});
			});

			it("Fails when title is empty", done => {
				const articleProps = {
					title: "",
					body: "New Lorem Ipsum article body",
					category: "signaleren"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, user, articleProps))
							.catch(error => {
								expect(error).to.haveOwnProperty("message", "Validation failed: title: Title is required!");
								done();
							});
					});
			});

			it("Fails when body is empty", done => {
				const articleProps = {
					title: "New article title",
					body: "",
					category: "signaleren"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.catch(error => {
									expect(error).to.haveOwnProperty("message", "Validation failed: body: Body is required!");
									done();
								}));
					});
			});

			it("Fails when category is empty", done => {
				const articleProps = {
					title: "New article title",
					body: "A new Lorem Ipsum article body",
					category: ""
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.catch(error => {
									expect(error).to.haveOwnProperty("message", "Validation failed: category: Category is required!");
									done();
								}));
					});
			});

			it("Passes when only title is modified", done => {
				const articleProps = {
					title: "another Article title"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.then(articleDocument => {
									const article = articleDocument._doc;
									expect(article).to.haveOwnProperty("_id");
									expect(article._id.toString()).to.equal(articleId.toString());
									expect(article).to.haveOwnProperty("title", "another Article title");
									expect(article).to.haveOwnProperty("body", "New Lorem Ipsum article body");
									expect(article).to.haveOwnProperty("category", "signaleren");
									expect(article).to.haveOwnProperty("owner");
									expect(article.owner.toString()).to.equal(user._id.toString());
									expect(article).to.haveOwnProperty("date");
									expect(article.date.toJSON()).to.equal(oldArticle.date);
									expect(article).to.haveOwnProperty("verified", true);
									done();
								}));
					});
			});

			it("Passes when only body is modified", done => {
				const articleProps = {
					body: "A brand new Lorem Ipsum article body"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.then(articleDocument => {
									const article = articleDocument._doc;
									expect(article).to.haveOwnProperty("_id");
									expect(article._id.toString()).to.equal(articleId.toString());
									expect(article).to.haveOwnProperty("title", "new Article title");
									expect(article).to.haveOwnProperty("body", "A brand new Lorem Ipsum article body");
									expect(article).to.haveOwnProperty("category", "signaleren");
									expect(article).to.haveOwnProperty("owner");
									expect(article.owner.toString()).to.equal(user._id.toString());
									expect(article).to.haveOwnProperty("date");
									expect(article.date.toJSON()).to.equal(oldArticle.date);
									expect(article).to.haveOwnProperty("verified", true);
									done();
								}));
					});
			});

			it("Passes when only category is modified", done => {
				const articleProps = {
					category: "diagnostiek"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.then(articleDocument => {
									const article = articleDocument._doc;
									expect(article).to.haveOwnProperty("_id");
									expect(article._id.toString()).to.equal(articleId.toString());
									expect(article).to.haveOwnProperty("title", "new Article title");
									expect(article).to.haveOwnProperty("body", "New Lorem Ipsum article body");
									expect(article).to.haveOwnProperty("category", "diagnostiek");
									expect(article).to.haveOwnProperty("owner");
									expect(article.owner.toString()).to.equal(user._id.toString());
									expect(article).to.haveOwnProperty("date");
									expect(article.date.toJSON()).to.equal(oldArticle.date);
									expect(article).to.haveOwnProperty("verified", true);
									done();
								}));
					});
			});

			it("Passes when an Administrator tries to update", done => {
				const articleProps = {
					category: "diagnostiek"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						UserService.registerAdministrator({
							firstName: "Test",
							lastName: "Admin",
							emailAddress: "other.admin@tripleProblematiek.nl",
							function: "Test Function",
							password: "T3stP@ssw0rd!"
						}).then(() =>
							User.findOne({ emailAddress: "other.admin@tripleProblematiek.nl" })
								.then(user => ArticleService.updateArticle(articleId, { ...user._doc, id: user._id }, articleProps)
									.then(articleDocument => {
										const article = articleDocument._doc;
										expect(article).to.haveOwnProperty("_id");
										expect(article._id.toString()).to.equal(articleId.toString());
										expect(article).to.haveOwnProperty("title", "new Article title");
										expect(article).to.haveOwnProperty("body", "New Lorem Ipsum article body");
										expect(article).to.haveOwnProperty("category", "diagnostiek");
										expect(article).to.haveOwnProperty("owner");
										expect(article.owner.toString()).to.equal(oldArticle.owner.toString());
										expect(article).to.haveOwnProperty("date");
										expect(article.date.toJSON()).to.equal(oldArticle.date);
										expect(article).to.haveOwnProperty("verified", true);
										done();
									})));
					});
			});

			it("Fails when another user tries to update", done => {
				const articleProps = {
					category: "diagnostiek"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						UserService.registerUser({
							firstName: "Test",
							lastName: "User",
							emailAddress: "other.user@tripleProblematiek.nl",
							function: "Test Function",
							password: "T3stP@ssw0rd!"
						}).then(() =>
							User.findOne({ emailAddress: "other.user@tripleProblematiek.nl" })
								.then(user => ArticleService.updateArticle(articleId, { ...user._doc, id: user._id }, articleProps)
									.catch(error => {
										expect(error).to.equal("There is no article with this _id, or you do not have the rights to change it!");
										done();
									})));
					});
			});
		});

		describe("Get unverified Articles", () => {
			beforeEach(done => {
				User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
					.then(storedUser => {
						Article.create({
							title: "Test article 1",
							body: "Lorem Ipsum",
							category: "signaleren",
							owner: storedUser._id,
							verified: true
						}).then(() => Article.create({
							title: "Test article 2",
							body: "Lorem Ipsum",
							category: "signaleren",
							owner: storedUser._id,
							verified: true
						})).then(() => Article.create({
							title: "Test article 3",
							body: "Lorem Ipsum",
							category: "signaleren",
							owner: storedUser._id,
							verified: false
						})).then(() => done());
					});
			});
			
			it("Passes if all unverified articles are shown", done => {
				ArticleService.getUnverified()
					.then(articles => {
						expect(articles).to.have.lengthOf(1);
						expect(articles[0]._doc).haveOwnProperty("owner");
						expect(articles[0].owner._doc).to.haveOwnProperty("emailAddress", "one.user@tripleProblematiek.nl");
						done();
					});
			});
		});

		describe("Get many Articles", () => {
			beforeEach(done => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(storedUser => {
						Article.create({
							title: "Test article 1",
							body: "Lorem Ipsum",
							category: "signaleren",
							owner: storedUser._id,
							verified: true
						}).then(() => Article.create({
							title: "Unique Article Title",
							body: "Lorem Ipsum",
							category: "signaleren",
							owner: storedUser._id,
							verified: true
						})).then(() => Article.create({
							title: "Test article 3",
							body: "Lorem Ipsum",
							category: "signaleren",
							owner: storedUser._id,
							verified: false
						})).then(() => done());
					});
			});
			
			it("Passes if all verified articles are shown", done => {
				ArticleService.getArticles()
					.then(articles => {
						expect(articles).to.have.lengthOf(2);
						expect(articles[0]._doc).haveOwnProperty("owner");
						expect(articles[0].owner._doc).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
						done();
					});
			});

			it("Passes if only the right articles are returned", done => {
				ArticleService.getArticles("Unique")
					.then(articles => {
						expect(articles).to.have.lengthOf(1);
						expect(articles[0]._doc).haveOwnProperty("owner");
						expect(articles[0]._doc).to.haveOwnProperty("title", "Unique Article Title");
						expect(articles[0].owner._doc).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
						done();
					});
			});
		});

		describe("Get one Article", () => {
			let articleId;

			beforeEach(done => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(storedUser => {
						Article.create({
							title: "Test article 1",
							body: "Lorem Ipsum",
							category: "signaleren",
							owner: storedUser._id,
							verified: true
						}).then(article => {
							articleId = article._id;
							Article.create({
								title: "Test article 2",
								body: "Lorem Ipsum",
								category: "signaleren",
								owner: storedUser._id,
								verified: false
							});
						}).then(() => Article.create({
							title: "Test article 3",
							body: "Lorem Ipsum",
							category: "signaleren",
							owner: storedUser._id,
							verified: false
						})).then(() => done());
					});
			});
			
			it("Passes if article exists", done => {
				ArticleService.getArticle(articleId)
					.then(articleDocument => {
						const article = articleDocument._doc;
						expect(article).to.haveOwnProperty("title", "Test article 1");
						expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
						expect(article).to.haveOwnProperty("category", "signaleren");
						expect(article).to.haveOwnProperty("owner");
						expect(article.owner._doc).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
						expect(article).to.haveOwnProperty("date");
						expect(article).to.haveOwnProperty("verified", true);
						done();
					});
			});

			it("Fails if article doesn't exist", done => {
				ArticleService.getArticle(new ObjectId())
					.catch(error => {
						expect(error).to.equal("There is no article with this _id!");
						done();
					});
			});
		});

		describe("Using the verify service, ", () => {
			let articleId;

			beforeEach(done => {
				User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
					.then(storedUser =>
						ArticleService.createArticle({
							title: "Test article 1",
							body: "Lorem Ipsum",
							category: "signaleren"
						}, { id: storedUser._id }))
					.then(article => {
						articleId = article._id;
						done();
					});
			});

			it("Verifies an article", done => {
				ArticleService.getArticle(articleId).then(articleDocument => {
					const article = articleDocument._doc;
					expect(article).to.haveOwnProperty("title", "Test article 1");
					expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
					expect(article).to.haveOwnProperty("category", "signaleren");
					expect(article).to.haveOwnProperty("owner");
					expect(article).to.haveOwnProperty("date");
					expect(article).to.haveOwnProperty("verified", false);
					return User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" });
				}).then(storedUser =>
					ArticleService.verify(articleId, { administrator: storedUser.administrator })
				).then(articleDocument => {
					const article = articleDocument._doc;
					expect(article).to.haveOwnProperty("title", "Test article 1");
					expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
					expect(article).to.haveOwnProperty("category", "signaleren");
					expect(article).to.haveOwnProperty("owner");
					expect(article).to.haveOwnProperty("date");
					expect(article).to.haveOwnProperty("verified", true);
					done();
				});
			});

			it("Fails if article doesn't exist", done => {
				ArticleService.getArticle(articleId).then(articleDocument => {
					const article = articleDocument._doc;
					expect(article).to.haveOwnProperty("title", "Test article 1");
					expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
					expect(article).to.haveOwnProperty("category", "signaleren");
					expect(article).to.haveOwnProperty("owner");
					expect(article).to.haveOwnProperty("date");
					expect(article).to.haveOwnProperty("verified", false);
					return User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" });
				}).then(storedUser =>
					ArticleService.verify(new ObjectId(), { administrator: storedUser.administrator })
				).catch(error => {
					expect(error).to.equal("There is no article with this _id!");
					done();
				});
			});

			it("Fails if user is not an administrator", done => {
				ArticleService.getArticle(articleId).then(articleDocument => {
					const article = articleDocument._doc;
					expect(article).to.haveOwnProperty("title", "Test article 1");
					expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
					expect(article).to.haveOwnProperty("category", "signaleren");
					expect(article).to.haveOwnProperty("owner");
					expect(article).to.haveOwnProperty("date");
					expect(article).to.haveOwnProperty("verified", false);
					return User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" });
				}).then(storedUser =>
					ArticleService.verify(articleId, { administrator: storedUser.administrator })
				).catch(error => {
					expect(error).to.equal("You must be an administrator to verify an article!");
					done();
				});
			});
		});

		describe("Using the myArticles service, ", () => {
			beforeEach(done => {
				User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
					.then(storedUser =>
						ArticleService.createArticle({
							title: "Test article 1",
							body: "Lorem Ipsum",
							category: "signaleren"
						}, { id: storedUser._id }))
					.then(article => {
						articleId = article._id;
						
						return User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" });
					}).then(storedUser =>
						ArticleService.createArticle({
							title: "Test article 2",
							body: "Lorem Ipsum",
							category: "signaleren"
						}, { id: storedUser._id }))
					.then(() => done());
			});

			it("Gets an array with only the articles this user created", done => {
				User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
					.then(user => ArticleService.myArticles({ id: user._id })
						.then(articles => {
							expect(articles).to.have.lengthOf(1);
							const article = articles[0]._doc;
							expect(article).to.haveOwnProperty("title", "Test article 1");
							expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
							expect(article).to.haveOwnProperty("category", "signaleren");
							expect(article).to.haveOwnProperty("owner");
							expect(article).to.haveOwnProperty("date");
							expect(article).to.haveOwnProperty("verified", false);
							done();
						}));
			});

			it("Returns an empty array if user has no articles", done => {
				ArticleService.myArticles({ id: new ObjectId() })
					.then(articles => {
						expect(articles).to.be.empty;
						done();
					});
			});
		});
	});

	describe("For the Article routes run by a normal User", () => {
		let token;

		beforeEach(done => {
			UserService.registerUser({
				firstName: "Test",
				lastName: "User",
				emailAddress: "one.user@tripleProblematiek.nl",
				function: "Test Function",
				password: "T3stP@ssw0rd!"
			}).then(({ newToken, firstName, lastName }) => {
				token = newToken;
				done();
			});
		});

		describe("Using the createArticle service, ", () => {
			it("Creates a new Article", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
						.then(user => {
							ArticleService.createArticle({
								title: "Test Title",
								body: "This is a test body",
								category: "signaleren"
							}, { id: user._id }).then(articleDocument => {
								const article = articleDocument._doc;
								expect(article).to.haveOwnProperty("title", "Test Title");
								expect(article).to.haveOwnProperty("body", "This is a test body");
								expect(article).to.haveOwnProperty("category", "signaleren");
								expect(article).to.haveOwnProperty("owner", user._id);
								expect(article).to.haveOwnProperty("date");
								expect(article).to.haveOwnProperty("verified", false);
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
						});
				});
			});

			it("Fails if required value is empty or just contains whitespace characters", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								title: "     			\n",
								body: "",
								category: "signaleren"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: title: Title is required!, body: Body is required!");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if title is missing", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								body: "This is a test body",
								category: "signaleren"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: title: Title is required!");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if body is missing", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								title: "Test Title",
								category: "signaleren"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: body: Body is required!");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if category is missing", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								title: "Test Title",
								body: "This is a test body"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: category: Category is required!");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if category is not a correct enum", done => {
				Article.countDocuments().then(oldCount => {
					User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
						.then(admin => {
							ArticleService.createArticle({
								title: "Test Title",
								body: "This is a test body",
								category: "Wrong Category"
							}, { id: admin._id }).catch( error => {
								expect(error).to.haveOwnProperty("message", "article validation failed: category: `Wrong Category` is not a valid enum value for path `category`.");
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount);
									done();
								});
							});
						});
				});
			});

			it("Fails if the creating user doesn't exist", done => {
				Article.countDocuments().then(oldCount => {
					ArticleService.createArticle({
						title: "Test Title",
						body: "This is a test body",
						category: "Wrong Category"
					}, { id: new ObjectId() }).catch( error => {
						expect(error).to.equal("This user does not exist!");
						Article.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
				});
			});
		});

		describe("Using the updateArticle service", () => {
			beforeEach(done => {
				User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
					.then(user => ArticleService.createArticle({
						title: "new Article title",
						body: "New Lorem Ipsum article body",
						category: "signaleren",
					}, { id: user._id }))
					.then(article => {
						articleId = article._id;
						done();
					});
			});

			it("Passes if the article got successfully updated", done => {
				const articleProps = {
					title: "Brand new Article title",
					body: "Ipsum article body",
					category: "diagnostiek"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.then(articleDocument => {
									const article = articleDocument._doc;
									expect(article).to.haveOwnProperty("_id");
									expect(article._id.toString()).to.equal(articleId.toString());
									expect(article).to.haveOwnProperty("title", "Brand new Article title");
									expect(article).to.haveOwnProperty("body", "Ipsum article body");
									expect(article).to.haveOwnProperty("category", "diagnostiek");
									expect(article).to.haveOwnProperty("owner");
									expect(article.owner.toString()).to.equal(user._id.toString());
									expect(article).to.haveOwnProperty("date");
									expect(article.date.toJSON()).to.equal(oldArticle.date);
									expect(article).to.haveOwnProperty("verified", false);
									done();
								}));
					});
			});

			it("Fails when title is empty", done => {
				const articleProps = {
					title: "",
					body: "New Lorem Ipsum article body",
					category: "signaleren"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, user, articleProps))
							.catch(error => {
								expect(error).to.haveOwnProperty("message", "Validation failed: title: Title is required!");
								done();
							});
					});
			});

			it("Fails when body is empty", done => {
				const articleProps = {
					title: "New article title",
					body: "",
					category: "signaleren"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.catch(error => {
									expect(error).to.haveOwnProperty("message", "Validation failed: body: Body is required!");
									done();
								}));
					});
			});

			it("Fails when category is empty", done => {
				const articleProps = {
					title: "New article title",
					body: "A new Lorem Ipsum article body",
					category: ""
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.catch(error => {
									expect(error).to.haveOwnProperty("message", "Validation failed: category: Category is required!");
									done();
								}));
					});
			});

			it("Passes when only title is modified", done => {
				const articleProps = {
					title: "another Article title"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.then(articleDocument => {
									const article = articleDocument._doc;
									expect(article).to.haveOwnProperty("_id");
									expect(article._id.toString()).to.equal(articleId.toString());
									expect(article).to.haveOwnProperty("title", "another Article title");
									expect(article).to.haveOwnProperty("body", "New Lorem Ipsum article body");
									expect(article).to.haveOwnProperty("category", "signaleren");
									expect(article).to.haveOwnProperty("owner");
									expect(article.owner.toString()).to.equal(user._id.toString());
									expect(article).to.haveOwnProperty("date");
									expect(article.date.toJSON()).to.equal(oldArticle.date);
									expect(article).to.haveOwnProperty("verified", false);
									done();
								}));
					});
			});

			it("Passes when only body is modified", done => {
				const articleProps = {
					body: "A brand new Lorem Ipsum article body"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.then(articleDocument => {
									const article = articleDocument._doc;
									expect(article).to.haveOwnProperty("_id");
									expect(article._id.toString()).to.equal(articleId.toString());
									expect(article).to.haveOwnProperty("title", "new Article title");
									expect(article).to.haveOwnProperty("body", "A brand new Lorem Ipsum article body");
									expect(article).to.haveOwnProperty("category", "signaleren");
									expect(article).to.haveOwnProperty("owner");
									expect(article.owner.toString()).to.equal(user._id.toString());
									expect(article).to.haveOwnProperty("date");
									expect(article.date.toJSON()).to.equal(oldArticle.date);
									expect(article).to.haveOwnProperty("verified", false);
									done();
								}));
					});
			});

			it("Passes when only category is modified", done => {
				const articleProps = {
					category: "diagnostiek"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
							.then(user => ArticleService.updateArticle(articleId, { ...user, id: user._id }, articleProps)
								.then(articleDocument => {
									const article = articleDocument._doc;
									expect(article).to.haveOwnProperty("_id");
									expect(article._id.toString()).to.equal(articleId.toString());
									expect(article).to.haveOwnProperty("title", "new Article title");
									expect(article).to.haveOwnProperty("body", "New Lorem Ipsum article body");
									expect(article).to.haveOwnProperty("category", "diagnostiek");
									expect(article).to.haveOwnProperty("owner");
									expect(article.owner.toString()).to.equal(user._id.toString());
									expect(article).to.haveOwnProperty("date");
									expect(article.date.toJSON()).to.equal(oldArticle.date);
									expect(article).to.haveOwnProperty("verified", false);
									done();
								}));
					});
			});

			it("Passes when an Administrator tries to update", done => {
				const articleProps = {
					category: "diagnostiek"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						UserService.registerAdministrator({
							firstName: "Test",
							lastName: "Admin",
							emailAddress: "other.admin@tripleProblematiek.nl",
							function: "Test Function",
							password: "T3stP@ssw0rd!"
						}).then(() =>
							User.findOne({ emailAddress: "other.admin@tripleProblematiek.nl" })
								.then(user => ArticleService.updateArticle(articleId, { ...user._doc, id: user._id }, articleProps)
									.then(articleDocument => {
										const article = articleDocument._doc;
										expect(article).to.haveOwnProperty("_id");
										expect(article._id.toString()).to.equal(articleId.toString());
										expect(article).to.haveOwnProperty("title", "new Article title");
										expect(article).to.haveOwnProperty("body", "New Lorem Ipsum article body");
										expect(article).to.haveOwnProperty("category", "diagnostiek");
										expect(article).to.haveOwnProperty("owner");
										expect(article.owner.toString()).to.equal(oldArticle.owner.toString());
										expect(article).to.haveOwnProperty("date");
										expect(article.date.toJSON()).to.equal(oldArticle.date);
										expect(article).to.haveOwnProperty("verified", false);
										done();
									})));
					});
			});

			it("Fails when another user tries to update", done => {
				const articleProps = {
					category: "diagnostiek"
				};

				Article.findById(articleId)
					.then(oldArticle => {
						UserService.registerUser({
							firstName: "Test",
							lastName: "User",
							emailAddress: "other.user@tripleProblematiek.nl",
							function: "Test Function",
							password: "T3stP@ssw0rd!"
						}).then(() =>
							User.findOne({ emailAddress: "other.user@tripleProblematiek.nl" })
								.then(user => ArticleService.updateArticle(articleId, { ...user._doc, id: user._id }, articleProps)
									.catch(error => {
										expect(error).to.equal("There is no article with this _id, or you do not have the rights to change it!");
										done();
									})));
					});
			});
		});

		describe("Using the deleteArticle service", () => {
			beforeEach(done => {
				User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
					.then(user => ArticleService.createArticle({
						title: "new Article title",
						body: "New Lorem Ipsum article body",
						category: "signaleren",
					}, { id: user._id }))
					.then(article => {
						articleId = article._id;
						done();
					});
			});

			it("Passes if an article is successfully deleted", done => {
				Article.findById(articleId)
					.then(oldArticle => {
						User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
							.then(user => ArticleService.deleteArticle(articleId, { ...user, id: user._id })
								.then(articleDocument => {
									const article = articleDocument._doc;
									expect(article).to.haveOwnProperty("_id");
									expect(article._id.toString()).to.equal(articleId.toString());
									expect(article).to.haveOwnProperty("title", "new Article title");
									expect(article).to.haveOwnProperty("body", "New Lorem Ipsum article body");
									expect(article).to.haveOwnProperty("category", "signaleren");
									expect(article).to.haveOwnProperty("owner");
									expect(article.owner.toString()).to.equal(user._id.toString());
									expect(article).to.haveOwnProperty("date");
									expect(article.date.toJSON()).to.equal(oldArticle.date);
									expect(article).to.haveOwnProperty("verified", false);
									done();
								}));
					});
			});

			it("Fails when an other user tries to delete the article", done => {
				UserService.registerUser({
					firstName: "Test",
					lastName: "User",
					emailAddress: "other.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				}).then(() =>
					User.findOne({ emailAddress: "other.user@tripleProblematiek.nl" })
						.then(user => ArticleService.deleteArticle(articleId, { ...user._doc, id: user._id })
							.catch(error => {
								expect(error).to.equal("There is no article with this _id, or you do not have the rights to change it!");
								done();
							})));
			});
		});
	});
});
