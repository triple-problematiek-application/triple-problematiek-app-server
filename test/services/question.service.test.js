/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const chai = require("chai");
const expect = chai.expect;
const ObjectId = require("mongoose").Types.ObjectId;

// Models
const Question = require("../../src/models/question/question.model");
const User = require("../../src/models/user.model");

// Services
const QuestionService = require("../../src/services/question.service");
const UserService = require("../../src/services/user.service");

describe("In testing the question service, ", function() {
	this.retries(1);

	beforeEach(done => {
		UserService.registerAdministrator({
			firstName: "Test",
			lastName: "Admin",
			emailAddress: "one.admin@tripleProblematiek.nl",
			function: "Helper",
			password: "T3stP@ssw0rd!"
		}).then(({ newToken, firstName, lastName }) => {
			// token = newToken;
			done();
		});
	});

	describe("Using the createQuestion service, ", () => {
		it("Creates a new Question", done => {
			Question.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						QuestionService.createQuestion({
							title: "Test Title",
							message: "Test message",
							category: "Technisch"
						}, { id: admin._id }).then(questionDocument => {
							const question = questionDocument._doc;
							expect(question).to.haveOwnProperty("title", "Test Title");
							expect(question).to.haveOwnProperty("message", "Test message");
							expect(question).to.haveOwnProperty("category", "Technisch");
							expect(question).to.haveOwnProperty("answers").and.to.be.empty;
							expect(question).to.haveOwnProperty("likes").and.to.be.empty;
							expect(question).to.haveOwnProperty("owner", admin._id);
							expect(question).to.haveOwnProperty("date");
							Question.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount + 1);
								done();
							});
						});
					});
			});
		});

		it("Fails if required value is empty or just contains whitespace characters", done => {
			Question.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						QuestionService.createQuestion({
							title: "     		\n",
							message: "",
							category: "Technisch"
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "question validation failed: title: Title is required!, message: Message is required!");
							Question.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Fails if title is missing", done => {
			Question.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						QuestionService.createQuestion({
							message: "Test message",
							category: "Technisch"
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "question validation failed: title: Title is required!");
							Question.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Fails if description is missing", done => {
			Question.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						QuestionService.createQuestion({
							title: "Test Title",
							category: "Technisch"
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "question validation failed: message: Message is required!");
							Question.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Fails if category is missing", done => {
			Question.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						QuestionService.createQuestion({
							title: "Test Title",
							message: "Test message"
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "question validation failed: category: Category is required!");
							Question.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Fails if category is invalid", done => {
			Question.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						QuestionService.createQuestion({
							title: "Test Title",
							message: "Test message",
							category: "Invalid Category"
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "question validation failed: category: `Invalid Category` is not a valid enum value for path `category`.");
							Question.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Fails if the creating user doesn't exist", done => {
			Question.countDocuments().then(oldCount => {
				QuestionService.createQuestion({
					title: "Test Title",
					message: "Test message",
					category: "Technisch"
				}, { id: new ObjectId() }).catch( error => {
					expect(error).to.haveOwnProperty("message", "question validation failed: owner: Owner must exist!");
					Question.countDocuments().then(newCount => {
						expect(newCount).to.equal(oldCount);
						done();
					});
				});
			});
		});
	});
});
