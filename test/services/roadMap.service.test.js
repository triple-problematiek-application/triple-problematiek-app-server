/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const chai = require("chai");
const expect = chai.expect;
const ObjectId = require("mongoose").Types.ObjectId;

// Models
const RoadMap = require("../../src/models/roadMap.model");
const User = require("../../src/models/user.model");

// Services
const RoadMapService = require("../../src/services/roadMap.service");
const UserService = require("../../src/services/user.service");

describe("In testing the roadMap service, ", function() {
	this.retries(1);

	beforeEach(done => {
		UserService.registerAdministrator({
			firstName: "Test",
			lastName: "Admin",
			emailAddress: "one.admin@tripleProblematiek.nl",
			function: "Test Function",
			password: "T3stP@ssw0rd!"
		}).then(() => done());
	});

	describe("Using the createRoadMap service, ", () => {
		it("Creates a new RoadMap", done => {
			RoadMap.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						RoadMapService.createRoadMap({
							title: "Test Title",
							description: "Test description",
							steps: [],
							negativeBulletPoints: []
						}, { id: admin._id }).then(roadMapDocument => {
							const roadMap = roadMapDocument._doc;
							expect(roadMap).to.haveOwnProperty("title", "Test Title");
							expect(roadMap).to.haveOwnProperty("description", "Test description");
							expect(roadMap).to.haveOwnProperty("steps").and.to.be.empty;
							expect(roadMap).to.haveOwnProperty("owner", admin._id);
							expect(roadMap).to.haveOwnProperty("date");
							RoadMap.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount + 1);
								done();
							});
						});
					});
			});
		});

		it("Fails if required value is empty or just contains whitespace characters", done => {
			RoadMap.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						RoadMapService.createRoadMap({
							title: "     		\n",
							description: "",
							steps: []
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "roadMap validation failed: title: Title is required!, description: Description is required!");
							RoadMap.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Fails if title is missing", done => {
			RoadMap.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						RoadMapService.createRoadMap({
							description: "Test description",
							steps: []
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "roadMap validation failed: title: Title is required!");
							RoadMap.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Fails if description is missing", done => {
			RoadMap.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						RoadMapService.createRoadMap({
							title: "Test Title",
							steps: []
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "roadMap validation failed: description: Description is required!");
							RoadMap.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Succeeds with empty array if positive bullet points array is missing", done => {
			RoadMap.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						RoadMapService.createRoadMap({
							title: "Test Title",
							description: "Test description",
							negativeBulletPoints: []
						}, { id: admin._id }).then(roadMapDocument => {
							const roadMap = roadMapDocument._doc;
							expect(roadMap).to.haveOwnProperty("title", "Test Title");
							expect(roadMap).to.haveOwnProperty("description", "Test description");
							expect(roadMap).to.haveOwnProperty("steps").and.to.be.empty;
							expect(roadMap).to.haveOwnProperty("owner", admin._id);
							expect(roadMap).to.haveOwnProperty("date");
							RoadMap.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount + 1);
								done();
							});
						});
					});
			});
		});

		it("Fails if the creating user doesn't exist", done => {
			RoadMap.countDocuments().then(oldCount => {
				RoadMapService.createRoadMap({
					title: "Test Title",
					description: "Test description",
					steps: []
				}, { id: new ObjectId() }).catch( error => {
					expect(error).to.haveOwnProperty("message", "roadMap validation failed: owner: Owner must exist!");
					RoadMap.countDocuments().then(newCount => {
						expect(newCount).to.equal(oldCount);
						done();
					});
				});
			});
		});
	});

	describe("Using the getRoadMaps service", () => {
		beforeEach(done => {
			User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
				.then(storedUser => {
					RoadMap.create({
						title: "Test roadmap 1",
						description: "Lorem Ipsum",
						steps: [ "Ask for help to family members and/or friends", "Try to use the tips they gave" ],
						owner: storedUser._id
					}).then(() => RoadMap.create({
						title: "Test roadmap 1",
						description: "Lorem Ipsum",
						steps: [ "Ask for help to family members and/or friends", "Try to use the tips they gave" ],
						owner: storedUser._id
					})).then(() => RoadMap.create({
						title: "Test roadmap 1",
						description: "Lorem Ipsum",
						steps: [ "Ask for help to family members and/or friends", "Try to use the tips they gave" ],
						owner: storedUser._id
					})).then(() => done());
				});
		});

		it("Passes if RoadMaps are successfully retrieved", done => {
			RoadMapService.getRoadMaps()
				.then(roadMaps => {
					expect(roadMaps).to.have.lengthOf(3);
					expect(roadMaps[0]._doc).to.haveOwnProperty("owner");
					expect(roadMaps[0].owner._doc).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
					done();
				});
		});
	});

	describe("Get one RoadMap", () => {
		let roadMapId;

		beforeEach(done => {
			User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
				.then(storedUser => {
					RoadMap.create({
						title: "Test roadmap 1",
						description: "Lorem Ipsum",
						steps: [ "Ask for help to family members and/or friends", "Try to use the tips they gave" ],
						owner: storedUser._id
					}).then(roadMap => {
						roadMapId = roadMap._id;
						RoadMap.create({
							title: "Test roadmap 2",
							description: "Lorem Ipsum",
							steps: [ "Ask for help to family members and/or friends", "Try to use the tips they gave" ],
							owner: storedUser._id
						}).then(() => RoadMap.create({
							title: "Test roadmap 3",
							description: "Lorem Ipsum",
							steps: [ "Ask for help to family members and/or friends", "Try to use the tips they gave" ],
							owner: storedUser._id
						})).then(() => done());
					});
				});
		});

		it("Passes if roadMap does exist", done => {
			RoadMapService.getRoadMap(roadMapId)
				.then(roadMapDocument => {
					const roadMap = roadMapDocument._doc;
					expect(roadMap).to.haveOwnProperty("title", "Test roadmap 1");
					expect(roadMap).to.haveOwnProperty("description", "Lorem Ipsum");
					expect(roadMap).to.haveOwnProperty("steps").to.have.lengthOf(2);
					expect(roadMap.steps[0]).to.equal("Ask for help to family members and/or friends");
					expect(roadMap.steps[1]).to.equal("Try to use the tips they gave");
					expect(roadMap).to.haveOwnProperty("owner");
					expect(roadMap.owner._doc).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
					expect(roadMap).to.haveOwnProperty("date");
					done();
				});
		});

		it("Fails if article doesn't exist", done => {
			RoadMapService.getRoadMap(new ObjectId())
				.catch(error => {
					expect(error).to.equal("There is no roadMap with this _id!");
					done();
				});
		});
	});

	describe("Get one RoadMap", () => {
		let roadMapId;

		beforeEach(done => {
			User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
				.then(storedUser => {
					RoadMap.create({
						title: "Test roadmap 1",
						description: "Lorem Ipsum",
						steps: [ "Ask for help to family members and/or friends", "Try to use the tips they gave" ],
						owner: storedUser._id
					}).then(roadMap => {
						roadMapId = roadMap._id;
						RoadMap.create({
							title: "Test roadmap 2",
							description: "Lorem Ipsum",
							steps: [ "Ask for help to family members and/or friends", "Try to use the tips they gave" ],
							owner: storedUser._id
						}).then(() => RoadMap.create({
							title: "Test roadmap 3",
							description: "Lorem Ipsum",
							steps: [ "Ask for help to family members and/or friends", "Try to use the tips they gave" ],
							owner: storedUser._id
						})).then(() => done());
					});
				});
		});

		it("Passes if roadMap does exist", done => {
			RoadMapService.getRoadMap(roadMapId)
				.then(roadMapDocument => {
					const roadMap = roadMapDocument._doc;
					expect(roadMap).to.haveOwnProperty("title", "Test roadmap 1");
					expect(roadMap).to.haveOwnProperty("description", "Lorem Ipsum");
					expect(roadMap).to.haveOwnProperty("steps").to.have.lengthOf(2);
					expect(roadMap.steps[0]).to.equal("Ask for help to family members and/or friends");
					expect(roadMap.steps[1]).to.equal("Try to use the tips they gave");
					expect(roadMap).to.haveOwnProperty("owner");
					expect(roadMap.owner._doc).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
					expect(roadMap).to.haveOwnProperty("date");
					done();
				});
		});

		it("Fails if article doesn't exist", done => {
			RoadMapService.getRoadMap(new ObjectId())
				.catch(error => {
					expect(error).to.equal("There is no roadMap with this _id!");
					done();
				});
		});
	});
});