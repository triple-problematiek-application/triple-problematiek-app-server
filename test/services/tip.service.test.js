/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const chai = require("chai");
const expect = chai.expect;
const ObjectId = require("mongoose").Types.ObjectId;

// Models
const Tip = require("../../src/models/tip.model");
const User = require("../../src/models/user.model");

// Services
const TipService = require("../../src/services/tip.service");
const UserService = require("../../src/services/user.service");

describe("In testing the tip service, ", function() {
	this.retries(1);

	beforeEach(done => {
		UserService.registerAdministrator({
			firstName: "Test",
			lastName: "Admin",
			emailAddress: "one.admin@tripleProblematiek.nl",
			function: "Test Function",
			password: "T3stP@ssw0rd!"
		}).then(() => {
			done();
		});
	});

	describe("Using the createTip service, ", () => {
		it("Creates a new Tip", done => {
			Tip.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						TipService.createTip({
							title: "Test Title",
							description: "Test description",
							positiveBulletPoints: [],
							negativeBulletPoints: []
						}, { id: admin._id }).then(tipDocument => {
							const tip = tipDocument._doc;
							expect(tip).to.haveOwnProperty("title", "Test Title");
							expect(tip).to.haveOwnProperty("description", "Test description");
							expect(tip).to.haveOwnProperty("positiveBulletPoints").and.to.be.empty;
							expect(tip).to.haveOwnProperty("negativeBulletPoints").and.to.be.empty;
							expect(tip).to.haveOwnProperty("owner", admin._id);
							expect(tip).to.haveOwnProperty("date");
							Tip.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount + 1);
								done();
							});
						});
					});
			});
		});

		it("Fails if required value is empty or just contains whitespace characters", done => {
			Tip.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						TipService.createTip({
							title: "     		\n",
							description: "",
							positiveBulletPoints: [],
							negativeBulletPoints: []
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "tip validation failed: title: Title is required!, description: Description is required!");
							Tip.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Fails if title is missing", done => {
			Tip.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						TipService.createTip({
							description: "Test description",
							positiveBulletPoints: [],
							negativeBulletPoints: []
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "tip validation failed: title: Title is required!");
							Tip.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Fails if description is missing", done => {
			Tip.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						TipService.createTip({
							title: "Test Title",
							positiveBulletPoints: [],
							negativeBulletPoints: []
						}, { id: admin._id }).catch( error => {
							expect(error).to.haveOwnProperty("message", "tip validation failed: description: Description is required!");
							Tip.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount);
								done();
							});
						});
					});
			});
		});

		it("Succeeds with empty array if positive bullet points array is missing", done => {
			Tip.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						TipService.createTip({
							title: "Test Title",
							description: "Test description",
							negativeBulletPoints: []
						}, { id: admin._id }).then(tipDocument => {
							const tip = tipDocument._doc;
							expect(tip).to.haveOwnProperty("title", "Test Title");
							expect(tip).to.haveOwnProperty("description", "Test description");
							expect(tip).to.haveOwnProperty("positiveBulletPoints").and.to.be.empty;
							expect(tip).to.haveOwnProperty("negativeBulletPoints").and.to.be.empty;
							expect(tip).to.haveOwnProperty("owner", admin._id);
							expect(tip).to.haveOwnProperty("date");
							Tip.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount + 1);
								done();
							});
						});
					});
			});
		});

		it("Succeeds with empty array if negative bullet points array is missing", done => {
			Tip.countDocuments().then(oldCount => {
				User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
					.then(admin => {
						TipService.createTip({
							title: "Test Title",
							description: "Test description",
							positiveBulletPoints: []
						}, { id: admin._id }).then(tipDocument => {
							const tip = tipDocument._doc;
							expect(tip).to.haveOwnProperty("title", "Test Title");
							expect(tip).to.haveOwnProperty("description", "Test description");
							expect(tip).to.haveOwnProperty("positiveBulletPoints").and.to.be.empty;
							expect(tip).to.haveOwnProperty("negativeBulletPoints").and.to.be.empty;
							expect(tip).to.haveOwnProperty("owner", admin._id);
							expect(tip).to.haveOwnProperty("date");
							Tip.countDocuments().then(newCount => {
								expect(newCount).to.equal(oldCount + 1);
								done();
							});
						});
					});
			});
		});

		it("Fails if the creating user doesn't exist", done => {
			Tip.countDocuments().then(oldCount => {
				TipService.createTip({
					title: "Test Title",
					description: "Test description",
					positiveBulletPoints: [],
					negativeBulletPoints: []
				}, { id: new ObjectId() }).catch( error => {
					expect(error).to.haveOwnProperty("message", "tip validation failed: owner: Owner must exist!");
					Tip.countDocuments().then(newCount => {
						expect(newCount).to.equal(oldCount);
						done();
					});
				});
			});
		});
	});

	describe("Using the getTips service", () => {
		beforeEach(done => {
			User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
				.then(storedUser => {
					Tip.create({
						title: "Test tip 1",
						description: "Lorem Ipsum",
						positiveBulletPoints: [ "Good", "First" ],
						negativeBulletPoints: [ "Bad", "Last" ],
						owner: storedUser._id
					}).then(() => Tip.create({
						title: "Test tip 2",
						description: "Lorem Ipsum",
						positiveBulletPoints: [ "Good", "First" ],
						negativeBulletPoints: [ "Bad", "Last" ],
						owner: storedUser._id
					})).then(() => Tip.create({
						title: "Test tip 3",
						description: "Lorem Ipsum",
						positiveBulletPoints: [ "Good", "First" ],
						negativeBulletPoints: [ "Bad", "Last" ],
						owner: storedUser._id
					})).then(() => done());
				});
		});
		
		it("Passess if all tips are successfully retrieved", done => {
			TipService.getTips()
				.then(tips => {
					expect(tips).to.have.lengthOf(3);
					expect(tips[0]._doc).haveOwnProperty("owner");
					expect(tips[0].owner._doc).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
					done();
				});
		});
	});

	describe("Get one Tip", () => {
		let tipId;

		beforeEach(done => {
			User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
				.then(storedUser => {
					Tip.create({
						title: "Test tip 1",
						description: "Lorem Ipsum",
						positiveBulletPoints: [ "First", "Good" ],
						negativeBulletPoints: [ "Last", "Bad" ],
						owner: storedUser._id
					}).then(tip => {
						tipId = tip._id;
						Tip.create({
							title: "Test tip 2",
							description: "Lorem Ipsum",
							positiveBulletPoints: [ "First", "Good" ],
							negativeBulletPoints: [ "Last", "Bad" ],
							owner: storedUser._id
						}).then(() => Tip.create({
							title: "Test tip 3",
							description: "Lorem Ipsum",
							positiveBulletPoints: [ "First", "Good" ],
							negativeBulletPoints: [ "Last", "Bad" ],
							owner: storedUser._id
						})).then(() => done());
					});
				});
		});

		it("Passes if tip does exist", done => {
			TipService.getTip(tipId)
				.then(tipDocument => {
					const tip = tipDocument._doc;
					expect(tip).to.haveOwnProperty("title", "Test tip 1");
					expect(tip).to.haveOwnProperty("description", "Lorem Ipsum");
					expect(tip).to.haveOwnProperty("positiveBulletPoints").to.have.lengthOf(2);
					expect(tip.positiveBulletPoints[0]).to.equal("First");
					expect(tip.positiveBulletPoints[1]).to.equal("Good");
					expect(tip).to.haveOwnProperty("negativeBulletPoints").to.have.lengthOf(2);
					expect(tip.negativeBulletPoints[0]).to.equal("Last");
					expect(tip.negativeBulletPoints[1]).to.equal("Bad");
					expect(tip).to.haveOwnProperty("owner");
					expect(tip.owner._doc).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
					expect(tip).to.haveOwnProperty("date");
					done();
				});
		});

		it("Fails if tip does doesn't exist", done => {
			TipService.getTip(new ObjectId())
				.catch(error => {
					expect(error).to.equal("There is no tip with this _id!");
					done();
				});
		});
	});
});