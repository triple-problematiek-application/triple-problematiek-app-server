/* eslint-disable no-undef */

const config = require("../../src/config/config");
const requester = require("../../requester");
const chai = require("chai");
const expect = chai.expect;
const bcrypt = require("bcrypt");
const app = require("../../src/app.js");

//User Model
const User = require("../../src/models/user.model");
//Middleware
const authenticationMiddleware = require("../../src/middleware/authentication.middleware");
//Service for token signing
const UserService = require("../../src/services/user.service");

const customURL = "/test/api/authenticatedRoute";

describe("In testing the authentication middleware", function() {
	this.retries(1);

	//The token to be send to the server
	let token;

	this.beforeAll(() =>
		app.get(customURL, authenticationMiddleware, (_, res) => res.sendStatus(200)));

	this.beforeEach(done => {
		bcrypt.hash("T3stP@ssw0rd!", config.security.bcrypt.saltRounds).then(hashedPassword =>
			User.create({
				emailAddress: "one.admin@tripleProblematiek.nl",
				firstName: "Test",
				lastName: "Admin",
				function: "Test Function",
				password: hashedPassword,
				administrator: true
			})
		).then(user => {
			token = UserService.signToken(user._doc).token;
			done();
		});
	});

	it("Passes if a valid token was given", () => {
		requester.get(customURL)
			.set("Authorization", `Bearer ${token}`)
			.end((_, res) => {
				expect(res).to.have.status(200);
			});
	});

	it("fails if a non valid token was given", () => {
		const invalidToken = token.replace(/./, ","); // * Invalidate the token by replacing the necessary periods with commas
		requester.get(customURL)
			.set("Authorization", `Bearer ${invalidToken}`)
			.end((_, res) => {
				expect(res).to.have.status(401);
				expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
				expect(res.body).to.haveOwnProperty("date", config.errorDate());
			});
	});

	it("fails if no token was given", () => {
		requester.get(customURL)
			.end((_, res) => {
				expect(res).to.have.status(401);
				expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
				expect(res.body).to.haveOwnProperty("date", config.errorDate());
			});
	});

	it("fails if jwt access token is unavailable", done => {
		const tempToken =  config.security.jwt.accessToken;
		config.security.jwt.accessToken = undefined;

		requester.get(customURL)
			.end((_, res) => {
				expect(res).to.have.status(401);
				expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
				expect(res.body).to.haveOwnProperty("date", config.errorDate());
				config.security.jwt.accessToken = tempToken;
				done();
			});
	});
});