/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const chai = require("chai");
const requester = require("../../requester");
const expect = chai.expect;
const bcrypt = require("bcrypt");
const ObjectId = require("mongoose").Types.ObjectId;

const config = require("../../src/config/config");
const logger = config.logger;

// Models
const RoadMap = require("../../src/models/roadMap.model");
const User = require("../../src/models/user.model");

// Services
const UserService = require("../../src/services/user.service");

const baseRoute = "/api/roadmap";

describe("In testing the tip routes", function() {
	this.retries(1);

	let roadMapId;
	let storedUser;
	let authToken;

	const createErrorResponse = (properties, statusCode, errorMessage, done) => {
		RoadMap.countDocuments().then(oldCount => {
			requester.post(baseRoute)
				.set("Authorization", `Bearer ${authToken}`)
				.send(properties)
				.end((err, res) => {
					expect(res).to.have.status(statusCode);
					expect(res.body).to.haveOwnProperty("error", errorMessage);
					expect(res.body).to.haveOwnProperty("date",config.errorDate());
					RoadMap.countDocuments().then(newCount => {
						expect(newCount).to.equal(oldCount);
						done();
					});
				});
		});
	};
    
	beforeEach(done => {
		bcrypt.hash("T3stP@ssw0rd!", config.security.bcrypt.saltRounds).then(hashedPassword =>
			User.create({
				firstName: "Test",
				lastName: "Admin",
				emailAddress: "one.admin@tripleProblematiek.nl",
				function: "Test Function",
				password: hashedPassword,
				administrator: true
			}).then(user => {
				storedUser = user;
				authToken = UserService.signToken(user._doc).token;

				return RoadMap.create({
					title: "New test roadmap",
					description: "Lorem Ipsum",
					steps: [],
					owner: storedUser._id
				});
			}).then(newRoadMap => {
				roadMapId = newRoadMap._id;

				done();
			}).catch(error => {
				logger.error("Something went wong");
				logger.error(error);
			}));
	});

	describe("Create a new RoadMap", () => {
		it("Passes if the roadMap got successfully created", done => {
			const roadMapProps = {
				title: "Test roadmap",
				description: "A beautiful test roadMap to add Lorem Ipsum",
				steps: []
			};

			RoadMap.countDocuments().then(oldCount => {
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${authToken}`)
					.send(roadMapProps)
					.end((err, res) => {
						RoadMap.findOne({ title: "Test roadmap" })
							.then(roadMap => {
								expect(res).to.have.status(201);
								expect(res.body).to.haveOwnProperty("steps").to.be.empty;
								expect(res.body).to.haveOwnProperty("date");
								expect(res.body).to.haveOwnProperty("_id", roadMap._id.toString());
								expect(res.body).to.haveOwnProperty("title", "Test roadmap");
								expect(res.body).to.haveOwnProperty("description", "A beautiful test roadMap to add Lorem Ipsum");
								expect(res.body).to.haveOwnProperty("owner", storedUser._id.toString());
								RoadMap.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
					});
			});
		});

		it("Fails when not authorized", done => {
			const roadMapProps = {
				title: "Test roadmap",
				description: "A beautiful test roadMap to add Lorem Ipsum",
				steps: []
			};

			requester.post(baseRoute)
				.send(roadMapProps)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("Fails when title is missing", done => {
			const roadMapProps = {
				description: "A beautiful test roadMap to add Lorem Ipsum",
				steps: []
			};

			createErrorResponse(
				roadMapProps,
				400,
				"roadMap validation failed: title: Title is required!",
				done
			);
		});

		it("Fails when description is missing", done => {
			const roadMapProps = {
				title: "Test roadmap",
				steps: []
			};

			createErrorResponse(
				roadMapProps,
				400,
				"roadMap validation failed: description: Description is required!",
				done
			);
		});

		it("Passes if steps is missing", done => {
			const roadMapProps = {
				title: "Test roadmap",
				description: "A beautiful test roadMap to add Lorem Ipsum",
			};
			
			RoadMap.countDocuments().then(oldCount => {
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${authToken}`)
					.send(roadMapProps)
					.end((err, res) => {
						RoadMap.findOne({ title: "Test roadmap" })
							.then(roadMap => {
								expect(res).to.have.status(201);
								expect(res.body).to.haveOwnProperty("steps").and.have.to.be.empty;
								expect(res.body).to.haveOwnProperty("date");
								expect(res.body).to.haveOwnProperty("_id", roadMap._id.toString());
								expect(res.body).to.haveOwnProperty("title", "Test roadmap");
								expect(res.body).to.haveOwnProperty("description", "A beautiful test roadMap to add Lorem Ipsum");
								expect(res.body).to.haveOwnProperty("owner", storedUser._id.toString());
								RoadMap.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
					});
			});
		});

		it("Fails if the owner does not exist", done => {
			const roadMapProps = {
				title: "Test roadmap",
				description: "A beautiful test roadMap to add Lorem Ipsum",
				steps: []
			};

			RoadMap.countDocuments().then(oldCount => {
				const { token, firstName, lastName } = UserService.signToken({
					_id: new ObjectId(),
					firstName: "Test",
					lastName: "User",
					emailAddress: "other.user@tripleProblematiek.nl",
					administrator: false
				});

				requester.post(baseRoute)
					.set("Authorization", `Bearer ${token}`)
					.send(roadMapProps)
					.end((err, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", "roadMap validation failed: owner: Owner must exist!");
						RoadMap.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});
	});

	describe("Get many RoadMaps", () => {
		beforeEach(done => {
			RoadMap.create({
				title: "Test roadmap 1",
				description: "Lorem Ipsum",
				steps: [],
				owner: storedUser._id
			}).then(() => RoadMap.create({
				title: "Test roadmap 2",
				description: "Lorem Ipsum",
				steps: [],
				owner: storedUser._id
			})).then(() => RoadMap.create({
				title: "Test roadmap 3",
				description: "Lorem Ipsum",
				steps: [],
				owner: storedUser._id
			})).then(() => done());
		});

		it("Passes if all roadMaps are shown", done => {
			requester.get(baseRoute)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("roadMaps").to.have.lengthOf(4);
					done();
				});
		});
	});

	describe("Get one RoadMap", () => {
		let roadMapId;

		beforeEach(done => {
			RoadMap.create({
				title: "Test roadmap 1",
				description: "Lorem Ipsum",
				steps: [],
				owner: storedUser._id
			}).then(brandNewRoadMap => {
				roadMapId = brandNewRoadMap._id;
				RoadMap.create({
					title: "Test roadmap 2",
					description: "Lorem Ipsum",
					steps: [],
					owner: storedUser._id
				}).then(() => RoadMap.create({
					title: "Test roadmap 3",
					description: "Lorem Ipsum",
					steps: [],
					owner: storedUser._id
				})).then(() => done());
			});
		});

		it("Passes if roadMap exists", done => {
			requester.get(`${baseRoute}/${roadMapId}`)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("title", "Test roadmap 1");
					expect(res.body).to.haveOwnProperty("description", "Lorem Ipsum");
					expect(res.body).to.haveOwnProperty("steps").to.be.empty;
					expect(res.body).to.haveOwnProperty("owner");
					expect(res.body.owner).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
					expect(res.body).to.haveOwnProperty("date");
					done();
				});
		});

		it("Fails if roadMap doesn't exist", done => {
			requester.get(`${baseRoute}/${new ObjectId()}`)
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "There is no roadMap with this _id!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});
});