/* eslint-disable no-undef */
const chai = require("chai");
const requester = require("../../requester");
const expect = chai.expect;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const config = require("../../src/config/config");

// Models
const User = require("../../src/models/user.model");

const baseRoute = "/api";

describe("In testing the user routes", function() {
	this.retries(1);

	describe("In testing the administrator routes", () => {
		beforeEach(done => {
			bcrypt.hash("T3stP@ssw0rd!", config.security.bcrypt.saltRounds).then(hashedPassword =>
				User.create({
					emailAddress: "one.admin@tripleProblematiek.nl",
					firstName: "Test",
					lastName: "Admin",
					function: "Test Function",
					password: hashedPassword,
					administrator: true
				})).then(() =>
				done());
		});
	
		describe("Register a new administrator", () => {
			it("Passes if administrator is successfully registered", done => {
				const adminProps = {
					emailAddress: "test.admin@test.io",
					firstName: "Test",
					lastName: "Admin",
					function: "Test Function",
					password: "Password1&"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/administrator`)
						.send(adminProps)
						.end((err, res) => {
							User.findOne({ emailAddress: "test.admin@test.io" })
								.then(admin => {
									expect(res).to.have.status(201);
									expect(res.body).to.haveOwnProperty("token").and.to.be.a("string");
									expect(res.body).to.haveOwnProperty("firstName", adminProps.firstName);
									expect(res.body).to.haveOwnProperty("lastName", adminProps.lastName);
									const tokenPayload = jwt.verify(res.body.token, config.security.jwt.accessToken);
									expect(tokenPayload).to.haveOwnProperty("id", admin._id.toString());
									expect(tokenPayload).to.haveOwnProperty("emailAddress", adminProps.emailAddress);
									expect(tokenPayload).to.haveOwnProperty("administrator", true);
									expect(tokenPayload).to.haveOwnProperty("iat").and.to.be.a("number");
									User.countDocuments().then(newCount => {
										expect(newCount).to.equals(oldCount + 1);
										done();
									});
								});
						});
				});
			});

			it("Fails if firstName is missing", done => {
				const adminProps = {
					emailAddress: "test.admin@test.io",
					lastName: "Admin",
					function: "Test Function",
					password: "Password1&"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/administrator`)
						.send(adminProps)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: firstName: First name is required!");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});

			it("Fails if lastName is missing", done => {
				const adminProps = {
					emailAddress: "test.admin@test.io",
					firstName: "Test",
					function: "Test Function",
					password: "Password1&"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/administrator`)
						.send(adminProps)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: lastName: Last name is required!");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});

			it("Fails if email is missing", done => {
				const adminProps = {
					firstName: "Test",
					lastName: "Admin",
					function: "Test Function",
					password: "Password1&"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/administrator`)
						.send(adminProps)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: emailAddress: Email address is required!");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});

			it("Fails if function is missing", done => {
				const adminProps = {
					firstName: "Test",
					lastName: "Admin",
					emailAddress: "test.admin@test.io",
					password: "T3stP@ssw0rd!"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/administrator`)
						.send(adminProps)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: function: Function is required!");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});
			
			it("Fails if password is missing", done => {
				const adminProps = {
					firstName: "Test",
					lastName: "Admin",
					emailAddress: "other.admin@tripleProblematiek.nl",
					function: "Test Function"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/administrator`)
						.send(adminProps)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "This password does not conform to the requirements!");
							expect(res.body).to.haveOwnProperty("date");
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});
			
			it("Fails if password does not match requirements", done => {
				const adminProps = {
					firstName: "Test",
					lastName: "Admin",
					emailAddress: "test.admin@test.io",
					function: "Test Function",
					password: "Password1"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/administrator`)
						.send(adminProps)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "This password does not conform to the requirements!");
							expect(res.body).to.haveOwnProperty("date");
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});
	
			it("Fails when email address is already taken", done => {
				const adminProps = {
					firstName: "Test",
					lastName: "Admin",
					emailAddress: "one.admin@tripleProblematiek.nl",
					function: "Test Function",
					password: "Password1&"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/administrator`)
						.send(adminProps)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: emailAddress: This Email address already has an account!");
							expect(res.body).to.haveOwnProperty("date");
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});
		});
	
		describe("Login to an existing administrator", () => {
			it("Passes if logging into administrator is successful", done => {
				const adminProps = {
					firstName: "Test",
					lastName: "Admin",
					emailAddress: "one.admin@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};
	
				requester.put(`${baseRoute}/login`)
					.send(adminProps)
					.end((err, res) => {
						User.findOne({ emailAddress: "one.admin@tripleProblematiek.nl" })
							.then(admin => {
								expect(res).to.have.status(200);
								expect(res.body).to.haveOwnProperty("token").and.to.be.a("string");
								expect(res.body).to.haveOwnProperty("firstName", adminProps.firstName);
								expect(res.body).to.haveOwnProperty("lastName", adminProps.lastName);
								const tokenPayload = jwt.verify(res.body.token, config.security.jwt.accessToken);
								expect(tokenPayload).to.haveOwnProperty("id", admin._id.toString());
								expect(tokenPayload).to.haveOwnProperty("emailAddress", admin.emailAddress);
								expect(tokenPayload).to.haveOwnProperty("administrator", true);
								expect(tokenPayload).to.haveOwnProperty("iat").and.to.be.a("number");
								done();
							});
					});
			});
	
			it("Fails if email is missing", done => {
				const adminProps = {
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};
	
				requester.put(`${baseRoute}/login`)
					.send(adminProps)
					.end((err, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "Cannot read property 'password' of null");
						expect(res.body).to.haveOwnProperty("date");
						done();
					});
			});
	
			it("Fails if password is missing", done => {
				const adminProps = {
					emailAddress: "one.admin@tripleProblematiek.nl"
				};
	
				requester.put(`${baseRoute}/login`)
					.send(adminProps)
					.end((err, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "data and hash arguments required");
						expect(res.body).to.haveOwnProperty("date");
						done();
					});
			});
	
			it("Fails if emailAddress is incorrect", done => {
				const adminProps = {
					emailAddress: "two.admin@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};
	
				requester.put(`${baseRoute}/login`)
					.send(adminProps)
					.end((err, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "Cannot read property 'password' of null");
						expect(res.body).to.haveOwnProperty("date");
						done();
					});
			});
	
			it("Fails if password is incorrect", done => {
				const adminProps = {
					emailAddress: "one.admin@tripleProblematiek.nl",
					function: "Test Function",
					password: "P3stw0rd!"
				};
	
				requester.put(`${baseRoute}/login`)
					.send(adminProps)
					.end((err, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "Incorrect Password!");
						expect(res.body).to.haveOwnProperty("date");
						done();
					});
			});
		});
	});

	describe("For the User routes", function() {

		this.beforeEach(done => {
			bcrypt.hash("T3stP@ssw0rd!", config.security.bcrypt.saltRounds).then(hashedPassword =>
				User.create({
					emailAddress: "one.user@tripleProblematiek.nl",
					firstName: "Test",
					lastName: "User",
					function: "Test Function",
					password: hashedPassword,
					administrator: false
				})).then(() => done());
		});

		describe("A POST to /api/user", () => {
			it("Registers a new user and returns a JWT token", done => {
				const userProperties = {
					emailAddress: "other.user@tripleProblematiek.nl",
					firstName: "Test",
					lastName: "User",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};

				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/user`)
						.send(userProperties)
						.end((_, res) => {
							expect(res).to.have.status(201);
							expect(res.body).to.haveOwnProperty("firstName", userProperties.firstName);
							expect(res.body).to.haveOwnProperty("lastName", userProperties.lastName);
							expect(res.body).to.haveOwnProperty("token").and.to.be.a("string");
							const tokenPayload = jwt.verify(res.body.token, config.security.jwt.accessToken);
							expect(tokenPayload).to.haveOwnProperty("emailAddress", "other.user@tripleProblematiek.nl");
							expect(tokenPayload).to.haveOwnProperty("administrator", false);
							expect(tokenPayload).to.haveOwnProperty("iat").and.to.be.a("number");
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount + 1);
								done();
							});
						});
				});
			});

			it("Fails if email address is missing", done => {
				const userProperties = {
					firstName: "Test",
					lastName: "User",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};

				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/user`)
						.send(userProperties)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: emailAddress: Email address is required!");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});
			it("Fails if firstName is missing", done => {
				const userProperties = {
					emailAddress: "other.user@tripleProblematiek.nl",
					lastName: "User",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/user`)
						.send(userProperties)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: firstName: First name is required!");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});
			it("Fails if lastName is missing", done => {
				const userProperties = {
					emailAddress: "other.user@tripleProblematiek.nl",
					firstName: "Test",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/user`)
						.send(userProperties)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: lastName: Last name is required!");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});

			it("Fails if function is missing", done => {
				const adminProps = {
					firstName: "Test",
					lastName: "User",
					emailAddress: "other.user@tripleProblematiek.nl",
					password: "T3stP@ssw0rd!"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/user`)
						.send(adminProps)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: function: Function is required!");
							expect(res.body).to.haveOwnProperty("date", config.errorDate());
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});

			it("Fails if password is missing", done => {
				const userProperties = {
					firstName: "Test",
					lastName: "User",
					emailAddress: "other.user@tripleProblematiek.nl"
				};
	
				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/user`)
						.send(userProperties)
						.end((err, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.ownProperty("error", "This password does not conform to the requirements!");
							expect(res.body).to.ownProperty("date", config.errorDate());
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});

			it("Fails if password is invalid", done => {
				const userProperties = {
					firstName: "Test",
					lastName: "User",
					emailAddress: "other.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "InvalidPassword"
				};

				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/user`)
						.send(userProperties)
						.end((_, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.ownProperty("error", "This password does not conform to the requirements!");
							expect(res.body).to.ownProperty("date");
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});

			it("Fails if email address already exists", done => {
				const userProperties = {
					firstName: "Test",
					lastName: "User",
					emailAddress: "one.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};

				User.countDocuments().then(oldCount => {
					requester.post(`${baseRoute}/user`)
						.send(userProperties)
						.end((_, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "user validation failed: emailAddress: This Email address already has an account!");
							expect(res.body).to.ownProperty("date");
							User.countDocuments().then(newCount => {
								expect(newCount).to.equals(oldCount);
								done();
							});
						});
				});
			});
		});

		describe("A PUT to /api/user", () => {
			it("Logs a user in and returns a JWT token", done => {
				const userProperties = {
					firstName: "Test",
					lastName: "User",
					emailAddress: "one.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};

				requester.put(`${baseRoute}/login`)
					.send(userProperties)
					.end((_, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("firstName", userProperties.firstName);
						expect(res.body).to.haveOwnProperty("lastName", userProperties.lastName);
						expect(res.body).to.haveOwnProperty("token").and.to.be.a("string");
						const tokenPayload = jwt.verify(res.body.token, config.security.jwt.accessToken);
						expect(tokenPayload).to.haveOwnProperty("emailAddress", "one.user@tripleProblematiek.nl");
						expect(tokenPayload).to.haveOwnProperty("administrator", false);
						expect(tokenPayload).to.haveOwnProperty("iat").and.to.be.a("number");
						done();
					});
			});

			it("Fails if email address is missing", done => {
				const userProperties = {
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};

				requester.put(`${baseRoute}/login`)
					.send(userProperties)
					.end((err, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "Cannot read property 'password' of null");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						done();
					});
			});

			it("Fails if password is missing", done => {
				const userProperties = {
					emailAddress: "other.user@tripleProblematiek.nl"
				};
	
				requester.post(`${baseRoute}/user`)
					.send(userProperties)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.ownProperty("error", "This password does not conform to the requirements!");
						expect(res.body).to.ownProperty("date", config.errorDate());
						done();
					});
			});

			it("Fails if email address doesn't exists", done => {
				const userProperties = {
					emailAddress: "other.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "T3stP@ssw0rd!"
				};

				requester.put(`${baseRoute}/login`)
					.send(userProperties)
					.end((_, res) => {
						expect(res).to.have.status(401);
						expect(res.body).to.haveOwnProperty("error", "Cannot read property 'password' of null");
						expect(res.body).to.ownProperty("date");
						done();
					});
			});

			it("Fails if password is wrong", done => {
				const userProperties = {
					emailAddress: "one.user@tripleProblematiek.nl",
					function: "Test Function",
					password: "Wrong Password"
				};

				requester.post(`${baseRoute}/user`)
					.send(userProperties)
					.end((_, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", "This password does not conform to the requirements!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						done();
					});
			});
		});
	});
});
