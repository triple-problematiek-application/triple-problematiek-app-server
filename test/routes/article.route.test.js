/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const chai = require("chai");
const requester = require("../../requester");
const expect = chai.expect;
const bcrypt = require("bcrypt");
const ObjectId = require("mongoose").Types.ObjectId;

const config = require("../../src/config/config");
const logger = config.logger;

// Models
const Article = require("../../src/models/article.model");
const User = require("../../src/models/user.model");

// Services
const UserService = require("../../src/services/user.service");
const ArticleService = require("../../src/services/article.service");

const baseRoute = "/api/article";

describe("In testing the article routes", function() {
	this.retries(1);

	let articleId;
	let storedUser;
	let authToken;
	let userAuthToken;
	let userCreatedArticleId;

	const createErrorResponse = (properties, statusCode, errorMessage, done) => {
		Article.countDocuments().then(oldCount => {
			requester.post(baseRoute)
				.set("Authorization", `Bearer ${authToken}`)
				.send(properties)
				.end((err, res) => {
					expect(res).to.have.status(statusCode);
					expect(res.body).to.haveOwnProperty("error", errorMessage);
					expect(res.body).to.haveOwnProperty("date",config.errorDate());
					Article.countDocuments().then(newCount => {
						expect(newCount).to.equal(oldCount);
						done();
					});
				});
		});
	};

	beforeEach(done => {
		bcrypt.hash("T3stP@ssw0rd!", config.security.bcrypt.saltRounds).then(hashedPassword =>
			User.create({
				firstName: "Test",
				lastName: "Admin",
				emailAddress: "one.admin@tripleProblematiek.nl",
				function: "Test Function",
				password: hashedPassword,
				administrator: true
			}).then(user => {
				storedUser = user;
				authToken = UserService.signToken(user._doc).token;

				return User.create({
					firstName: "Test",
					lastName: "User",
					emailAddress: "one.user@tripleProblematiek.nl",
					function: "Test Function",
					password: hashedPassword,
					administrator: false
				});
			}).then(user => {
				userAuthToken = UserService.signToken(user._doc).token;

				return Article.create({
					title: "NEW test article 2",
					body: "Lorem Ipsum",
					category: "signaleren",
					owner: user._id,
					verified: false
				});
			}).then( newArticle => {
				userCreatedArticleId = newArticle._id;

				return Article.create({
					title: "NEW test article",
					body: "Lorem Ipsum",
					category: "signaleren",
					owner: storedUser._id,
					verified: false
				});
			}).then(newArticle => {
				articleId = newArticle._id;
				done();
			}).catch(error => {
				logger.error("Something went wrong");
				logger.error(error);
			}));
	});

	describe("Create a new Article", () => {
		it("Passes if the article got successfully created", done => {
			const articleProps = {
				title: "Test article",
				body: "A beautiful test article to add Lorem Ipsum",
				category: "diagnostiek"
			};

			Article.countDocuments().then(oldCount => {
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${authToken}`)
					.send(articleProps)
					.end((err, res) => {
						Article.findOne({ title: "Test article" })
							.then(article => {
								expect(res).to.have.status(201);
								expect(res.body).to.haveOwnProperty("date");
								expect(res.body).to.haveOwnProperty("verified", true);
								expect(res.body).to.haveOwnProperty("_id", article._id.toString());
								expect(res.body).to.haveOwnProperty("title", "Test article");
								expect(res.body).to.haveOwnProperty("body", "A beautiful test article to add Lorem Ipsum");
								expect(res.body).to.haveOwnProperty("category", "diagnostiek"); 
								expect(res.body).to.haveOwnProperty("owner", storedUser._id.toString());
								Article.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
					});
			});
		});

		it("Fails when not authorized", done => {
			const articleProps = {
				title: "Test article",
				body: "A beautiful test article to add Lorem Ipsum",
				category: "diagnostiek"
			};

			requester.post(baseRoute)
				.send(articleProps)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("Fails when title is missing", done => {
			const articleProps = {
				body: "A beautiful test article to add Lorem Ipsum",
				category: "diagnostiek"
			};

			createErrorResponse(
				articleProps,
				"400",
				"article validation failed: title: Title is required!",
				done
			);
		});

		it("Fails when body is missing", done => {
			const articleProps = {
				title: "Test article",
				category: "diagnostiek"
			};

			createErrorResponse(
				articleProps, 
				400, 
				"article validation failed: body: Body is required!", 
				done
			);
		});

		it("Fails when category is missing", done => {
			const articleProps = {
				title: "Test article",
				body: "A beautiful test article to add Lorem Ipsum"
			};

			createErrorResponse(
				articleProps,
				400,
				"article validation failed: category: Category is required!",
				done
			);
		});

		it("Fails if the owner doesn't exist", done => {
			const articleProps = {
				title: "Test article",
				body: "A beautiful test article to add Lorem Ipsum",
				category: "diagnostiek"
			};

			Article.countDocuments().then(oldCount => {
				const token = UserService.signToken({
					_id: new ObjectId(),
					firstName: "Test",
					lastName: "User",
					emailAddress: "other.user@tripleProblematiek.nl",
					administrator: false
				}).token;
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${token}`)
					.send(articleProps)
					.end((err, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", "This user does not exist!");
						expect(res.body).to.haveOwnProperty("date",config.errorDate());
						Article.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});
	});

	describe("Update an existing Article", () => {
		it("Passes with article is successfully updated", done => {
			const articleProps = {
				title: "A new updated article title",
				body: "A brand new Lorem Ipsum body for article",
				category: "begeleiding"
			};

			requester.put(`${baseRoute}/${articleId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(articleProps)
				.end((err, res) => {
					Article.findById(articleId)
						.then(article => {
							expect(res).to.have.status(200);
							expect(res.body).to.haveOwnProperty("_id", article._id.toString());
							expect(res.body).to.haveOwnProperty("title", "A new updated article title");
							expect(res.body).to.haveOwnProperty("body", "A brand new Lorem Ipsum body for article");
							expect(res.body).to.haveOwnProperty("category", "begeleiding");
							expect(res.body).to.haveOwnProperty("owner", storedUser._id.toString());
							expect(res.body).to.haveOwnProperty("date");
							done();
						});
				});
		});

		it("Fails when user is not authorized", done => {
			const articleProps = {
				title: "A new updated article title",
				body: "A brand new Lorem Ipsum body for article",
				category: "begeleiding"
			};

			requester.put(`${baseRoute}/${articleId}`)
				.send(articleProps)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
		
		it("Fails when title is empty", done => {
			const articleProps = {
				title: "",
				body: "A brand new Lorem Ipsum body for article",
				category: "begeleiding"
			};

			requester.put(`${baseRoute}/${articleId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(articleProps)
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Validation failed: title: Title is required!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("Fails when body is empty", done => {
			const articleProps = {
				title: "A brand new article title",
				body: "",
				category: "begeleiding"
			};

			requester.put(`${baseRoute}/${articleId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(articleProps)
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Validation failed: body: Body is required!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("Fails when category is empty", done => {
			const articleProps = {
				title: "A brand new article title",
				body: "A brand new Lorem Ipsum body for article",
				category: ""
			};

			requester.put(`${baseRoute}/${articleId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send(articleProps)
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Validation failed: category: Category is required!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});

		it("Passes when an Administrator tries to update", done => {
			const articleProps = {
				title: "A new updated article title",
				body: "A brand new Lorem Ipsum body for article",
				category: "begeleiding"
			};

			UserService.registerAdministrator({
				firstName: "Test",
				lastName: "Admin",
				emailAddress: "other.admin@tripleProblematiek.nl",
				function: "Test Function",
				password: "T3stP@ssw0rd!"
			}).then(({ token, firstName, lastName }) => {
				requester.put(`${baseRoute}/${articleId}`)
					.set("Authorization", `Bearer ${token}`)
					.send(articleProps)
					.end((err, res) => {
						Article.findById(articleId)
							.then(article => {
								expect(res).to.have.status(200);
								expect(res.body).to.haveOwnProperty("_id", article._id.toString());
								expect(res.body).to.haveOwnProperty("title", "A new updated article title");
								expect(res.body).to.haveOwnProperty("body", "A brand new Lorem Ipsum body for article");
								expect(res.body).to.haveOwnProperty("category", "begeleiding");
								expect(res.body).to.haveOwnProperty("owner", storedUser._id.toString());
								expect(res.body).to.haveOwnProperty("date");
								done();
							});
					});
			});
		});

		it("Fails when another user tries to update", done => {
			const articleProps = {
				category: "diagnostiek"
			};

			UserService.registerUser({
				firstName: "Test",
				lastName: "User",
				emailAddress: "other.user@tripleProblematiek.nl",
				function: "Test Function",
				password: "T3stP@ssw0rd!"
			}).then(({ token, firstName, lastName }) => {
				requester.put(`${baseRoute}/${articleId}`)
					.set("Authorization", `Bearer ${token}`)
					.send(articleProps)
					.end((err, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", "There is no article with this _id, or you do not have the rights to change it!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						done();
					});
			});
		});
	});

	describe("Get many Articles", () => {
		beforeEach(done => {
			Article.create({
				title: "Test article 1",
				body: "Lorem Ipsum",
				category: "signaleren",
				owner: storedUser._id,
				verified: true
			}).then(() => Article.create({
				title: "Unique Article Title",
				body: "Lorem Ipsum",
				category: "signaleren",
				owner: storedUser._id,
				verified: true
			})).then(() => Article.create({
				title: "Test article 3",
				body: "Lorem Ipsum",
				category: "signaleren",
				owner: storedUser._id,
				verified: false
			})).then(() => done());
		});

		it("Passes if all verified articles are shown", done => {
			requester.get(baseRoute)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("articles").to.have.lengthOf(2);
					done();
				});
		});

		it("Passes if only the right articles are returned", done => {
			requester.get(`${baseRoute}?title=Unique`)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("articles").to.have.lengthOf(1);
					expect(res.body.articles[0]).to.haveOwnProperty("title", "Unique Article Title");
					done();
				});
		});
	});

	describe("Get unverified Articles", () => {
		beforeEach(done => {
			Article.create({
				title: "Test article 1",
				body: "Lorem Ipsum",
				category: "signaleren",
				owner: storedUser._id,
				verified: true
			}).then(() => Article.create({
				title: "Test article 2",
				body: "Lorem Ipsum",
				category: "signaleren",
				owner: storedUser._id,
				verified: true
			})).then(() => Article.create({
				title: "Test article 3",
				body: "Lorem Ipsum",
				category: "signaleren",
				owner: storedUser._id,
				verified: false
			})).then(() => done());
		});

		it("Passes if all unverified articles are shown", done => {
			requester.get(`${baseRoute}/unverified`)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("articles").to.have.lengthOf(3);
					done();
				});
		});
	});

	describe("Get one Article", () => {
		let articleId;

		beforeEach(done => {
			Article.create({
				title: "Test article 1",
				body: "Lorem Ipsum",
				category: "signaleren",
				owner: storedUser._id
			}).then(article => {
				articleId = article._id;
				Article.create({
					title: "Test article 2",
					body: "Lorem Ipsum",
					category: "signaleren",
					owner: storedUser._id
				});
			}).then(() => Article.create({
				title: "Test article 3",
				body: "Lorem Ipsum",
				category: "signaleren",
				owner: storedUser._id
			})).then(() => done());
		});
		
		it("Passes if article exists", done => {
			requester.get(`${baseRoute}/${articleId}`)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("title", "Test article 1");
					expect(res.body).to.haveOwnProperty("body", "Lorem Ipsum");
					expect(res.body).to.haveOwnProperty("category", "signaleren");
					expect(res.body).to.haveOwnProperty("owner");
					expect(res.body.owner).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
					expect(res.body).to.haveOwnProperty("date");
					expect(res.body).to.haveOwnProperty("verified", false);
					done();
				});
		});

		it("Fails if article doesn't exist", done => {
			requester.get(`${baseRoute}/${new ObjectId()}`)
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "There is no article with this _id!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});

	describe("Delete an existing Article", () => {
		it("Passes if an article is successfully deleted", done => {
			requester.delete(`${baseRoute}/${articleId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					Article.findById(articleId)
						.then(article => {
							expect(res).to.have.status(200);
							expect(res.body).to.haveOwnProperty("_id", articleId._id.toString());
							expect(res.body).to.haveOwnProperty("title", "NEW test article");
							expect(res.body).to.haveOwnProperty("body", "Lorem Ipsum");
							expect(res.body).to.haveOwnProperty("category", "signaleren");
							expect(res.body).to.haveOwnProperty("owner", storedUser._id.toString());
							expect(res.body).to.haveOwnProperty("date");
							done();
						});
				});
		});

		it("Fails if the articleId does not exist", done => {
			requester.delete(`${baseRoute}/-432423`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Cast to ObjectId failed for value \"-432423\" at path \"_id\" for model \"article\"");
					expect(res.body).to.haveOwnProperty("date");
					done();
				});
		});

		it("Fails when user is not authorized", done => {
			requester.delete(`${baseRoute}/${articleId}`)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date");
					done();
				});
		});
	});

	describe("Verifying an article, ", () => {
		let articleId;

		beforeEach(done => {
			User.findOne({ emailAddress: "one.user@tripleProblematiek.nl" })
				.then(storedUser =>
					ArticleService.createArticle({
						title: "Test article 1",
						body: "Lorem Ipsum",
						category: "signaleren"
					}, { id: storedUser._id }))
				.then(article => {
					articleId = article._id;
					done();
				});
		});

		it("Verifies an article", done => {
			ArticleService.getArticle(userCreatedArticleId)
				.then(articleDocument => {
					const article = articleDocument._doc;
					expect(article).to.haveOwnProperty("title", "NEW test article 2");
					expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
					expect(article).to.haveOwnProperty("category", "signaleren");
					expect(article).to.haveOwnProperty("owner");
					expect(article).to.haveOwnProperty("date");
					expect(article).to.haveOwnProperty("verified", false);
					requester.put(`${baseRoute}/${userCreatedArticleId}/verify`)
						.set("Authorization", `Bearer ${authToken}`)
						.end((_, res) => {
							expect(res).to.have.status(200);
							expect(res.body).to.haveOwnProperty("_id", userCreatedArticleId._id.toString());
							expect(res.body).to.haveOwnProperty("title", "NEW test article 2");
							expect(res.body).to.haveOwnProperty("body", "Lorem Ipsum");
							expect(res.body).to.haveOwnProperty("category", "signaleren");
							expect(res.body).to.haveOwnProperty("owner");
							expect(res.body).to.haveOwnProperty("verified", true);
							expect(res.body).to.haveOwnProperty("date");
							done();
						});
				});
		});

		it("Fails if article doesn't exist", done => {
			ArticleService.getArticle(userCreatedArticleId)
				.then(articleDocument => {
					const article = articleDocument._doc;
					expect(article).to.haveOwnProperty("title", "NEW test article 2");
					expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
					expect(article).to.haveOwnProperty("category", "signaleren");
					expect(article).to.haveOwnProperty("owner");
					expect(article).to.haveOwnProperty("date");
					expect(article).to.haveOwnProperty("verified", false);
					requester.put(`${baseRoute}/${new ObjectId()}/verify`)
						.set("Authorization", `Bearer ${authToken}`)
						.end((_, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "There is no article with this _id!");
							expect(res.body).to.haveOwnProperty("date");
							done();
						});
				});
		});

		it("Fails if user is not an administrator", done => {
			ArticleService.getArticle(userCreatedArticleId)
				.then(articleDocument => {
					const article = articleDocument._doc;
					expect(article).to.haveOwnProperty("title", "NEW test article 2");
					expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
					expect(article).to.haveOwnProperty("category", "signaleren");
					expect(article).to.haveOwnProperty("owner");
					expect(article).to.haveOwnProperty("date");
					expect(article).to.haveOwnProperty("verified", false);
					requester.put(`${baseRoute}/${userCreatedArticleId}/verify`)
						.set("Authorization", `Bearer ${userAuthToken}`)
						.end((_, res) => {
							expect(res).to.have.status(400);
							expect(res.body).to.haveOwnProperty("error", "You must be an administrator to verify an article!");
							expect(res.body).to.haveOwnProperty("date");
							done();
						});
				});
		});
	});

	describe("Getting a users articles, ", () => {
		it("Returns only the articles created by this user", done => {
			requester.get(`${baseRoute}/myArticles`)
				.set("Authorization", `Bearer ${userAuthToken}`)
				.end((_, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("articles").to.have.lengthOf(1);
					const article = res.body.articles[0];
					expect(article).to.haveOwnProperty("title", "NEW test article 2");
					expect(article).to.haveOwnProperty("body", "Lorem Ipsum");
					expect(article).to.haveOwnProperty("category", "signaleren");
					expect(article).to.haveOwnProperty("owner");
					expect(article).to.haveOwnProperty("date");
					expect(article).to.haveOwnProperty("verified", false);
					done();
				});
		});

		it("Returns an empty array when the user doesn't have any articles", done => {
			const tempAuthToken = UserService.signToken({
				_id: new ObjectId(),
				firstName: "Temp",
				lastName: "User",
				emailAddress: "temp.user@tripleProblematiek.nl",
				administrator: false
			}).token;
			requester.get(`${baseRoute}/myArticles`)
				.set("Authorization", `Bearer ${tempAuthToken}`)
				.end((_, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("articles").to.have.lengthOf(0);
					done();
				});
		});
	});
});