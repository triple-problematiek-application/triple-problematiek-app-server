/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const chai = require("chai");
const requester = require("../../requester");
const expect = chai.expect;
const bcrypt = require("bcrypt");
const ObjectId = require("mongoose").Types.ObjectId;

const config = require("../../src/config/config");
const logger = config.logger;

// Models
const Tip = require("../../src/models/tip.model");
const User = require("../../src/models/user.model");

// Services
const UserService = require("../../src/services/user.service");

const baseRoute = "/api/tip";

describe("In testing the tip routes", function() {
	this.retries(1);

	let tipId;
	let storedUser;
	let authToken;

	const createErrorResponse = (properties, statusCode, errorMessage, done) => {
		Tip.countDocuments().then(oldCount => {
			requester.post(baseRoute)
				.set("Authorization", `Bearer ${authToken}`)
				.send(properties)
				.end((err, res) => {
					expect(res).to.have.status(statusCode);
					expect(res.body).to.haveOwnProperty("error", errorMessage);
					expect(res.body).to.haveOwnProperty("date",config.errorDate());
					Tip.countDocuments().then(newCount => {
						expect(newCount).to.equal(oldCount);
						done();
					});
				});
		});
	};

	beforeEach(done => {
		bcrypt.hash("T3stP@ssw0rd!", config.security.bcrypt.saltRounds).then(hashedPassword =>
			User.create({
				firstName: "Test",
				lastName: "Admin",
				emailAddress: "one.admin@tripleProblematiek.nl",
				function: "Test Function",
				password: hashedPassword,
				administrator: true
			}).then(user => {
				storedUser = user;
				authToken = UserService.signToken(user._doc).token;

				return Tip.create({
					title: "New test tip",
					description: "Lorem Ipsum",
					positiveBulletPoints: [],
					negativeBulletPoints: [],
					owner: storedUser._id
				});
			}).then(newTip => {
				tipId = newTip._id;

				done();
			}).catch(error => {
				logger.error("Something went wrong");
				logger.error(error);
			}));
	});

	describe("Create a new Tip", () => {
		it("Passes if the tip got successfully created", done => {
			const tipProps = {
				title: "Test tip",
				description: "A beautiful test tip to add Lorem Ipsum",
				positiveBulletPoints: [],
				negativeBulletPoints: []
			};

			Tip.countDocuments().then(oldCount => {
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${authToken}`)
					.send(tipProps)
					.end((err, res) => {
						Tip.findOne({ title: "Test tip" })
							.then(tip => {
								expect(res).to.have.status(201);
								expect(res.body).to.haveOwnProperty("positiveBulletPoints").and.have.to.be.empty;
								expect(res.body).to.haveOwnProperty("negativeBulletPoints").and.have.to.be.empty;
								expect(res.body).to.haveOwnProperty("date");
								expect(res.body).to.haveOwnProperty("_id", tip._id.toString());
								expect(res.body).to.haveOwnProperty("title", "Test tip");
								expect(res.body).to.haveOwnProperty("description", "A beautiful test tip to add Lorem Ipsum");
								expect(res.body).to.haveOwnProperty("owner", storedUser._id.toString());
								Tip.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
					});
			});
		});
        
		it("Fails when not authorized", done => {
			const tipProps = {
				title: "Test tip",
				description: "A test tip to add Lorem Ipsum",
				positiveBulletPoints: [],
				negativeBulletPoints: []
			};
            
			requester.post(baseRoute)
				.send(tipProps)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "User not authenticated!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
        
		it("Fails if title is missing", done => {
			const tipProps = {
				description: "A beautiful test tip to add Lorem Ipsum",
				positiveBulletPoints: [],
				negativeBulletPoints: []
			};

			createErrorResponse(
				tipProps,
				400,
				"tip validation failed: title: Title is required!",
				done
			);
		});

		it("Fails if description is missing", done => {
			const tipProps = {
				title: "Test tip",
				positiveBulletPoints: [],
				negativeBulletPoints: []
			};

			createErrorResponse(
				tipProps,
				400,
				"tip validation failed: description: Description is required!",
				done
			);
		});

		it("Passes if positiveBulletPoints is missing", done => {
			const tipProps = {
				title: "Test tip",
				description: "A test tip to add Lorem Ipsum",
				negativeBulletPoints: []
			};

			Tip.countDocuments().then(oldCount => {
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${authToken}`)
					.send(tipProps)
					.end((err, res) => {
						Tip.findOne({ title: "Test tip" })
							.then(tip => {
								expect(res).to.have.status(201);
								expect(res.body).to.haveOwnProperty("positiveBulletPoints").and.have.to.be.empty;
								expect(res.body).to.haveOwnProperty("negativeBulletPoints").and.have.to.be.empty;
								expect(res.body).to.haveOwnProperty("date");
								expect(res.body).to.haveOwnProperty("_id", tip._id.toString());
								expect(res.body).to.haveOwnProperty("title", "Test tip");
								expect(res.body).to.haveOwnProperty("description", "A test tip to add Lorem Ipsum");
								expect(res.body).to.haveOwnProperty("owner", storedUser._id.toString());
								Tip.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
					});
			});
		});

		it("Passes if negativeBulletPoints is missing", done => {
			const tipProps = {
				title: "Test tip",
				description: "A test tip to add Lorem Ipsum",
				positiveBulletPoints: []
			};

			Tip.countDocuments().then(oldCount => {
				requester.post(baseRoute)
					.set("Authorization", `Bearer ${authToken}`)
					.send(tipProps)
					.end((err, res) => {
						Tip.findOne({ title: "Test tip" })
							.then(tip => {
								expect(res).to.have.status(201);
								expect(res.body).to.haveOwnProperty("positiveBulletPoints").and.have.to.be.empty;
								expect(res.body).to.haveOwnProperty("negativeBulletPoints").and.have.to.be.empty;
								expect(res.body).to.haveOwnProperty("date");
								expect(res.body).to.haveOwnProperty("_id", tip._id.toString());
								expect(res.body).to.haveOwnProperty("title", "Test tip");
								expect(res.body).to.haveOwnProperty("description", "A test tip to add Lorem Ipsum");
								expect(res.body).to.haveOwnProperty("owner", storedUser._id.toString());
								Tip.countDocuments().then(newCount => {
									expect(newCount).to.equal(oldCount + 1);
									done();
								});
							});
					});
			});
		});

		it("Fails if the owner does not exist", done => {
			const tipProps = {
				title: "Test article",
				description: "A beautiful test article to add Lorem Ipsum",
				positiveBulletPoints: [],
				negativeBulletPoints: []
			};

			Tip.countDocuments().then(oldCount => {
				const token = UserService.signToken({
					_id: new ObjectId(),
					firstName: "Test",
					lastName: "User",
					emailAddress: "other.user@tripleProblematiek.nl",
					administrator: false
				}).token;

				requester.post(baseRoute)
					.set("Authorization", `Bearer ${token}`)
					.send(tipProps)
					.end((err, res) => {
						expect(res).to.have.status(400);
						expect(res.body).to.haveOwnProperty("error", "tip validation failed: owner: Owner must exist!");
						expect(res.body).to.haveOwnProperty("date", config.errorDate());
						Tip.countDocuments().then(newCount => {
							expect(newCount).to.equal(oldCount);
							done();
						});
					});
			});
		});
	});

	describe("Get many Tips", () => {
		beforeEach(done => {
			Tip.create({
				title: "Test tip 1",
				description: "Lorem Ipsum",
				positiveBulletPoints: [],
				negativeBulletPoints: [],
				owner: storedUser._id
			}).then(() => Tip.create({
				title: "Test tip 2",
				description: "Lorem Ipsum",
				positiveBulletPoints: [],
				negativeBulletPoints: [],
				owner: storedUser._id
			})).then(() => Tip.create({
				title: "Test tip 3",
				description: "Lorem Ipsum",
				positiveBulletPoints: [],
				negativeBulletPoints: [],
				owner: storedUser._id
			})).then(() => done());
		});

		it("Passes if all tips are shown", done => {
			requester.get(baseRoute)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("tips").to.have.lengthOf(4);
					done();
				});
		});
	});

	describe("Get one Tip", () => {
		let tipId;

		beforeEach(done => {
			Tip.create({
				title: "Test tip 1",
				description: "Lorem Ipsum",
				positiveBulletPoints: [ "Good", "First" ],
				negativeBulletPoints: [ "Bad", "Last" ],
				owner: storedUser._id
			}).then(tip => {
				tipId = tip._id;
				Tip.create({
					title: "Test tip 2",
					description: "Lorem Ipsum",
					positiveBulletPoints: [ "Good", "First" ],
					negativeBulletPoints: [ "Bad", "Last" ],
					owner: storedUser._id
				}).then(() => Tip.create({
					title: "Test tip 3",
					description: "Lorem Ipsum",
					positiveBulletPoints: [ "Good", "First" ],
					negativeBulletPoints: [ "Bad", "Last" ],
					owner: storedUser._id
				})).then(() => done());
			});
		});

		it("Passes if tip exists", done => {
			requester.get(`${baseRoute}/${tipId}`)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("title", "Test tip 1");
					expect(res.body).to.haveOwnProperty("description", "Lorem Ipsum");
					expect(res.body).to.haveOwnProperty("positiveBulletPoints").to.have.lengthOf(2);
					expect(res.body.positiveBulletPoints[0]).to.equal("Good");
					expect(res.body.positiveBulletPoints[1]).to.equal("First");
					expect(res.body).to.haveOwnProperty("negativeBulletPoints").to.have.lengthOf(2);
					expect(res.body.negativeBulletPoints[0]).to.equal("Bad");
					expect(res.body.negativeBulletPoints[1]).to.equal("Last");
					expect(res.body).to.haveOwnProperty("owner");
					expect(res.body.owner).to.haveOwnProperty("emailAddress", "one.admin@tripleProblematiek.nl");
					expect(res.body).to.haveOwnProperty("date");
					done();
				});
		});

		it("Fails if tip doesn't exist", done => {
			requester.get(`${baseRoute}/${new ObjectId()}`)
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "There is no tip with this _id!");
					expect(res.body).to.haveOwnProperty("date", config.errorDate());
					done();
				});
		});
	});
});