# Triple Problematiek Application Server
This is the back end for the "Triple Problematiek" application.\
The back end consists of an Express server, connected via Mongoose to a MongoDB database hosted on MongoDB Atlas.\
All of this is run using NodeJS.

This server is part of a project for the Agile SCRUM course in period 1.4 of the Computer Science bachelor at Avans University of Applied Science.

## Installation
Use the node package manager (npm) to install this project after cloning or downloading it.
```bash
npm install --production
npm start
```
Alternatively, the project can be run using nodemon, provided that the dev dependencies are installed:
```bash
npm install
npm run nodemon
```

## Environment Variables
The following environment variables must be defined. When running locally, this can be done in a .env file.\
* ACCESS_TOKEN_SECRET; The secret used for generating and verifying tokens with JSONWebToken.
* MONGODB_CONNECTION_URL; The URL at which the MongoDB database can be reached. (authentication included)
* MONGODB_TEST_CONNECTION_URL; (Development Only!) The URL at which the MongoDB test database can be reached. (authentication included)
* SALT_ROUNDS; (Optional!) The number of rounds that Bcrypt should use when hashing a password.
* LOG_LEVEL; (Optional!) The log level used by [tracer](https://www.npmjs.com/package/tracer "tracer on npm")
* SONAR_LOGIN_KEY; (Optional!) The secret as authentication for SonarQube (best alternative for `email` & `password`)
* SONAR_PROJECT_KEY; (Optional!) The secret which references to the project on SonarQube

## Usage
You clone the project, and run npm install and npm start to install and start the server at a given port.\
At that point, you can make calls to the /api endpoints and the server will respond.

## Contributing
No contributions will be allowed for this project.

## License
As of yet, no license has been added to this project.
