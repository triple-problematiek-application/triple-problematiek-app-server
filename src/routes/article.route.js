const router = require("express").Router();
const ArticleService = require("../services/article.service");
const authentication = require("../middleware/authentication.middleware");
const config = require("../config/config");

/**
 * Article create route
 * @param { title, body, category, ?cover } req Request with specified body
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with specified body
 */
router.post("/article", authentication, (req, res) => {
	ArticleService.createArticle(req.body, req.user)
		.then(result => res.status(201).send(result))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Article get many route
 * @param req Request with optional query param of title
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with an array of the specified body
 */
router.get("/article", (req, res) => {
	ArticleService.getArticles(req.query.title).then(articles => res.status(200).send({ articles }));
});

/**
 * Article get unverified route
 * @param req Request
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with an array of the specified body
 */
router.get("/article/unverified", (req, res) => {
	ArticleService.getUnverified(req.query.title).then(articles => res.status(200).send({ articles }));
});

/**
 * Article my articles route
 * @param req Request
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with an array of the specified body
 */
router.get("/article/myArticles", authentication, (req, res) => {
	ArticleService.myArticles(req.user, req.query.title)
		.then(articles => {
			res.status(200).send({ articles });
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Article get one route
 * @param { articleId } req Request with the articles' _id in the body
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with an article of the specified body
 */
router.get("/article/:id", (req, res) => {
	ArticleService.getArticle(req.params.id)
		.then(article => res.status(200).send(article))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Article update route
 * @param { title, body, category, ?cover } req Request with specified body
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with specified body
 */
router.put("/article/:id", authentication, (req, res) => {
	ArticleService.updateArticle(req.params.id, req.user, req.body)
		.then(article => {
			res.status(200).send(article);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Article delete route
 * @param { articleId } req Request with the articles' _id in the body
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with specified body
 */
router.delete("/article/:id", authentication, (req, res) => {
	ArticleService.deleteArticle(req.params.id, req.user)
		.then(article => {
			res.status(200).send(article);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Article verify route
 * @param req Request
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with specified body
 */
router.put("/article/:id/verify", authentication, (req, res) => {
	ArticleService.verify(req.params.id, req.user)
		.then(article => {
			res.status(200).send(article);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

module.exports = router;