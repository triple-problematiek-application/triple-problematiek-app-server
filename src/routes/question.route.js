const router = require("express").Router();
const questionService = require("../services/question.service");
const authentication = require("../middleware/authentication.middleware");
const config = require("../config/config");

router.post("/question", authentication, (req, res) => {
	questionService.createQuestion(req.body, req.user)
		.then(result => res.status(201).send(result))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

router.get("/question", (req, res) => {
	questionService.getQuestions()
		.then(questions => res.status(200).send({ questions }));
});

router.get("/question/:id", (req, res) => {
	questionService.getQuestion(req.params.id)
		.then(question => res.status(200).send(question))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

router.put("/question/:id", authentication, (req, res) => {
	questionService.updateQuestion(req.params.id, req.user, req.body)
		.then(question => res.status(200).send(question))
		.catch(error => res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() }));
});

router.delete("/question/:id", authentication, (req, res) => {
	questionService.deleteQuestion(req.params.id, req.user)
		.then(question => res.status(200).send(question))
		.catch(error => res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() }));
});

module.exports = router;