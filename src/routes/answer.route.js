const router = require("express").Router();
const answerService = require("../services/answer.service");
const authentication = require("../middleware/authentication.middleware");
const config = require("../config/config");

router.post("/answer", authentication, (req, res) => {
	answerService.createAnswer(req.body, req.user)
		.then(result => res.status(201).send(result))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

router.put("/answer/:questionID/:answerID", authentication, (req, res) => {
	answerService.updateAnswer(req.params.questionID, req.params.answerID, req.user)
		.then(question => res.status(200).send(question))
		.catch(error => res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() }));
}); 

router.delete("/answer/:questionID/:answerID", authentication, (req, res) => {
	answerService.deleteAnswer(req.params.questionID, req.params.answerID, req.user)
		.then(question => res.status(200).send(question))
		.catch(error => res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() }));
});

module.exports = router;