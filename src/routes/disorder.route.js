const router = require("express").Router();
const DisorderService = require("../services/disorder.service");
const authentication = require("../middleware/authentication.middleware");
const config = require("../config/config");

/**
 * Disorder create route
 * @param { title, definition, description, symptoms, dealingWith } req Request with specified body
 * @param { _id, title, definition, description, symptoms, dealingWith } res Response which the server returns with specified body
 */
router.post("/disorder", authentication, (req, res) => {
	DisorderService.createDisorder(req.body, req.user)
		.then(disorder => res.status(201).send(disorder))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Disorder update route
 * @param { ?title, ?definition, ?description, ?symptoms, ?dealingWith } req Request with specified body
 * @param { _id, title, definition, description, symptoms, dealingWith } res Response which the server returns with specified body
 */
router.put("/disorder/:id", authentication, (req, res) => {
	DisorderService.updateDisorder(req.params.id, req.user, req.body)
		.then(disorder => {
			res.status(200).send(disorder);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Disorder delete route
 * @param { disorderId } req Request with the disorders' _id in the body
 * @param { _id, title, definition, description, symptoms, dealingWith } res Response which the server returns with specified body
 */
router.delete("/disorder/:id", authentication, (req, res) => {
	DisorderService.deleteDisorder(req.params.id, req.user)
		.then(disorder => {
			res.status(200).send(disorder);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Disorder get many route
 * @param req Request with optional query param of title
 * @param { _id, title, definition, description, symptoms, dealingWith } res Response which the server returns with an array of the specified body
 */
router.get("/disorder", (req, res) => {
	DisorderService.getDisorders(req.query.title).then(disorders => res.status(200).send({ disorders }));
});

/**
 * Disorder get one route
 * @param req Request
 * @param { _id, title, definition, description, symptoms, dealingWith } res Response which the server returns with a disorder of the specified body
 */
router.get("/disorder/:id", (req, res) => {
	DisorderService.getDisorder(req.params.id)
		.then(disorder => res.status(200).send(disorder))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

module.exports = router;