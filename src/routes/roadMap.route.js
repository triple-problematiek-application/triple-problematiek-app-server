const router = require("express").Router();
const RoadMapService = require("../services/roadMap.service");
const authentication = require("../middleware/authentication.middleware");
const config = require("../config/config");

/**
 * RoadMap create route
 * @param { title, description, ?cover, steps } req Request with specified body
 * @param { _id, title, description, ?cover, steps owner, date } res Response which the server returns with specified body
 */
router.post("/roadmap", authentication, (req, res) => {
	RoadMapService.createRoadMap(req.body, req.user)
		.then(roadMap => res.status(201).send(roadMap))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * RoadMap update route
 * @param { title, body, category, ?cover } req Request with specified body
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with specified body
 */
router.put("/roadmap/:id", authentication, (req, res) => {
	RoadMapService.updateRoadMap(req.params.id, req.user, req.body)
		.then(roadMap => {
			res.status(200).send(roadMap);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * RoadMap delete route
 * @param { roadmapId } req Request with the roadmaps' _id in the body
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with specified body
 */
router.delete("/roadmap/:id", authentication, (req, res) => {
	RoadMapService.deleteRoadMap(req.params.id, req.user)
		.then(roadMap => {
			res.status(200).send(roadMap);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * RoadMap get many route
 * @param req Request with optional query param of title
 * @param { _id, title, description, ?cover, steps, owner, date } res Response which the server returns with an array of the sepcified body
 */
router.get("/roadmap", (req, res) => {
	RoadMapService.getRoadMaps(req.query.title).then(roadMaps => res.status(200).send({ roadMaps }));
});

/**
 * Roadmap my roadMaps route
 * @param req Request
 * @param { _id, title, description, ?cover, positiveBulletPoints, negativeBulletPoints, owner, date } res Response which the server returns with an array of the specified body
 */
router.get("/roadmap/myRoadmaps", authentication, (req, res) => {
	RoadMapService.myRoadMaps(req.user, req.query.title)
		.then(roadMaps => {
			res.status(200).send({ roadMaps });
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * RoadMap get one route
 * @param req Request
 * @param { _id, title, description, ?cover, steps, owner, date } res Response which the server returns with an roadMap of the specified body
 */
router.get("/roadmap/:id", (req, res) => {
	RoadMapService.getRoadMap(req.params.id)
		.then(roadMap => res.status(200).send(roadMap))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

module.exports = router;