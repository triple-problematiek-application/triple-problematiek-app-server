const router = require("express").Router();
const UserService = require("../services/user.service");
const authentication = require("../middleware/authentication.middleware");
const config = require("../config/config");
const logger = config.logger;

/**
 * Administrator register route
 * @param { firstName, lastName, emailAddress, password } req Request with specified body
 * @param { jwtToken, firstName, lastName } res Response which the server returns with specified body
 */
router.post("/administrator", (req, res) => UserService.registerAdministrator(req.body)
	.then(result => res.status(201).send(result))
	.catch(error => {
		logger.debug(error);

		res.status(400).send({
			error: error.message ? error.message : error,
			date: config.errorDate()
		});
	})
);

/**
 * User login route
 * @param { emailAddress, password } req Request with specified body
 * @param { jwtToken, firstname, lastName } res Response which the server returns with specified body
 */
router.put("/login", (req, res) => UserService.loginUser(req.body)
	.then(result => res.status(200).send(result))
	.catch(error => {
		logger.debug(error);

		res.status(401).send({
			error: error.message ? error.message : error,
			date: config.errorDate()
		});
	})
);

/**
 * User update route
 * @param { ?firstName, ?lastName, ?emailAddress, ?phoneNumber, ?function, ?password, ?profilePicture } req Request with specified body
 * @param { _id, firstName, lastName, emailAddress, phoneNumber, function, profilePicture } res Response which the server returns with specified body
 */
router.put("/user", authentication, (req, res) => {
	UserService.updateUser(req.user, req.body)
		.then(user => {
			res.status(200).send(user);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * User register route
 * @param { firstName, lastName, emailAddress, password } req Request with specified body
 * @param { jwtToken, firstname, lastName } res Response which the server returns with specified body
 */
router.post("/user", (req, res) => UserService.registerUser(req.body)
	.then(result => res.status(201).send(result))
	.catch(error => {
		logger.debug(error);

		res.status(400).send({
			error: error.message ? error.message : error,
			date: config.errorDate()
		});
	})
);

module.exports = router;