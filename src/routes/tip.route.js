const router = require("express").Router();
const TipService = require("../services/tip.service");
const authentication = require("../middleware/authentication.middleware");
const config = require("../config/config");

/**
 * Tip create route
 * @param { title, description, ?cover, positiveBulletPoints, negativeBulletPoints } req Request with specified body
 * @param { _id, title, description, ?cover, positiveBulletPoints, negativeBulletPoints, owner, date } res Response which the server returns with specified body
 */
router.post("/tip", authentication, (req, res) => {
	TipService.createTip(req.body, req.user)
		.then(tip => res.status(201).send(tip))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Tip update route
 * @param { title, body, category, ?cover } req Request with specified body
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with specified body
 */
router.put("/tip/:id", authentication, (req, res) => {
	TipService.updateTip(req.params.id, req.user, req.body)
		.then(tip => {
			res.status(200).send(tip);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Tip delete route
 * @param { tipId } req Request with the tips' _id in the body
 * @param { _id, title, body, category, ?cover, owner, date, verified } res Response which the server returns with specified body
 */
router.delete("/tip/:id", authentication, (req, res) => {
	TipService.deleteTip(req.params.id, req.user)
		.then(tip => {
			res.status(200).send(tip);
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Tip get many route
 * @param req Request with optional query param of title
 * @param { _id, title, description, ?cover, positiveBulletPoints, negativeBulletPoints, owner, date } res Response which the server returns with an array of the specified body
 */
router.get("/tip", (req, res) => {
	TipService.getTips(req.query.title).then(tips => res.status(200).send({ tips }));
});

/**
 * Tip my tips route
 * @param req Request
 * @param { _id, title, description, ?cover, positiveBulletPoints, negativeBulletPoints, owner, date } res Response which the server returns with an array of the specified body
 */
router.get("/tip/myTips", authentication, (req, res) => {
	TipService.myTips(req.user, req.query.title)
		.then(tips => {
			res.status(200).send({ tips });
		}).catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

/**
 * Tip get one route
 * @param req Request
 * @param { _id, title, description, ?cover, positiveBulletPoints, negativeBulletPoints, owner date } res Response which the server returns with a tip of the specified body
 */
router.get("/tip/:id", (req, res) => {
	TipService.getTip(req.params.id)
		.then(tip => res.status(200).send(tip))
		.catch(error => {
			res.status(400).send({ error: error.message ? error.message : error, date: config.errorDate() });
		});
});

module.exports = router;