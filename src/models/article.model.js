const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * The schema of articles
 */
const ArticleSchema = new Schema({
	title: {
		type: String,
		trim: true,
		required: [ true, "Title is required!" ]
	},
	body: {
		type: String,
		trim: true,
		required: [ true, "Body is required!" ]
	},
	category: {
		type: String,
		trim: true,
		required: [ true, "Category is required!" ],
		enum: [
			"signaleren",
			"diagnostiek",
			"begeleiding",
			"behandeling",
			"verwijzing"
		]
	},
	cover: {
		type: String, // The cover in a base64 encoding
	},
	owner: {
		type: Schema.Types.ObjectId, // The _id of the user
		ref: "user",
		required: [ true, "Owner is required!" ]
	},
	date: {
		type: Date,
		default: new Date(),
		get: date => date.toJSON()
	},
	verified: {
		type: Boolean,
		default: false
	}
});

const Article = mongoose.model("article", ArticleSchema);

module.exports = Article;
