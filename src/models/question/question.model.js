const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AnswerSchema = require("./answer.schema");
const User = require("../user.model");

/**
 * Schema for the questions a user can ask
 */
const QuestionSchema = new Schema({
	title: {
		type: String,
		trim: true,
		required: [ true, "Title is required!" ]
	},
	message: {
		type: String,
		trim: true,
		required: [ true, "Message is required!" ]
	},
	category: {
		type: String,
		trim: true,
		required: [ true, "Category is required!" ],
		enum: [
			"Technisch",
			"Niet Technisch"
		]
	},
	answers: {
		type: [ AnswerSchema ],
		default: []
	},
	likes: {
		type: [ Schema.Types.ObjectId ],
		ref: "user",
		default: []
	},
	owner: {
		type: Schema.Types.ObjectId, // The _id of the user
		ref: "user",
		required: [ true, "Owner is required!" ],
		validate: {
			validator: userId => User.exists({ _id: userId }),
			message: "Owner must exist!"
		}
	},
	date: {
		type: Date,
		default: new Date(),
		get: date => date.toJSON()
	}
});

const Question = mongoose.model("question", QuestionSchema);

module.exports = Question;
