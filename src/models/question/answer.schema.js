const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * Schema for the tips
 */
const AnswerSchema = new Schema({
	message: {
		type: String,
		trim: true,
		required: [ true, "Message is required!" ]
	},
	owner: {
		type: Schema.Types.ObjectId, // The _id of the user
		ref: "user",
		required: [ true, "Owner is required!" ]
	},
	date: {
		type: Date,
		default: new Date(),
		get: date => date.toJSON()
	}
});

module.exports = AnswerSchema;
