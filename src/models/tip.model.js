const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const User = require("./user.model");

/**
 * Schema for the tips
 */
const TipSchema = new Schema({
	title: {
		type: String,
		trim: true,
		required: [ true, "Title is required!" ]
	},
	description: {
		type: String,
		trim: true,
		required: [ true, "Description is required!" ]
	},
	cover: {
		type: String, // The cover in a base64 encoding
	},
	positiveBulletPoints: {
		type: [ String ],
		required: [ true, "Positive bullet points are required!" ]
	},
	negativeBulletPoints: {
		type: [ String ],
		required: [ true, "Negative bullet points are required!" ]
	},
	owner: {
		type: Schema.Types.ObjectId, // The _id of the user
		ref: "user",
		required: [ true, "Owner is required!" ],
		validate: {
			validator: userId => User.exists({ _id: userId }),
			message: "Owner must exist!"
		}
	},
	date: {
		type: Date,
		default: new Date(),
		get: date => date.toJSON()
	}
});

const Tip = mongoose.model("tip", TipSchema);

module.exports = Tip;