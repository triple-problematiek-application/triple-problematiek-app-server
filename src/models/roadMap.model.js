const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const User = require("./user.model");

/**
 * Schema for the roadMaps
 */
const RoadMapSchema = new Schema({
	title: {
		type: String,
		trim: true,
		required: [ true, "Title is required!" ]
	},
	description: {
		type: String,
		trim: true,
		required: [ true, "Description is required!" ]
	},
	cover: {
		type: String, // The cover in a base64 encoding
	},
	steps: {
		type: [ String ],
		required: [ true, "Positive bullet points are required!" ]
	},
	owner: {
		type: Schema.Types.ObjectId, // The _id of the user
		ref: "user",
		required: [ true, "Owner is required!" ],
		validate: {
			validator: userId => User.exists({ _id: userId }),
			message: "Owner must exist!"
		}
	},
	date: {
		type: Date,
		default: new Date(),
		get: date => date.toJSON()
	}
});

const RoadMap = mongoose.model("roadMap", RoadMapSchema);

module.exports = RoadMap;