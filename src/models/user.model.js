const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * The schema for users and administrators.
 * ! The user is an administrator when administrator === true
 * * The profile picture is the base64 encoded image received from the client
 */
const UserSchema = new Schema({
	firstName: {
		type: String,
		trim: true,
		required: [ true, "First name is required!" ]
	},
	lastName: {
		type: String,
		trim: true,
		required: [ true, "Last name is required!" ]
	},
	emailAddress: {
		type: String,
		trim: true,
		required: [ true, "Email address is required!" ],
		unique: true,
		// eslint-disable-next-line no-control-regex
		match: [ /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, 
			"This Email address does not conform to the requirements!" ]
	},
	phoneNumber: {
		type: String,
		match: [ /^[-\s]*((00[-\s]*|\+)31[-\s]*(\(?0\)?)?|0)[-\s]*[1-9][-\s]*([0-9][-\s]*){8}$/, "Phone number must be valid!" ],
	},
	function: {
		type: String,
		trim: true,
		required: [ true, "Function is required!" ]
	},
	password: {
		type: String,
		trim: true,
		required: [ true, "Password is required!" ],
		select: false
	},
	administrator: {
		type: Boolean,
		default: false
	},
	profilePicture: {
		type: String, // The image in a base64 encoding
		trim: true
	}
});

const User = mongoose.model("user", UserSchema);

UserSchema.path("emailAddress").validate((emailAddress) =>
	User.countDocuments({ emailAddress }).then(count => !count), "This Email address already has an account!");

module.exports = User;
