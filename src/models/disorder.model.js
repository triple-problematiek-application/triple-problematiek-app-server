const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * The schema of disorders
 */
const DisorderSchema = new Schema({
	title: {
		type: String,
		trim: true,
		required: [ true, "Title is required!" ]
	},
	definition: {
		type: String,
		trim: true,
		required: [ true, "Definition is required!" ]
	},
	description: {
		type: String,
		trim: true,
		required: [ true, "Description is required!" ]
	},
	symptoms: {
		type: [ String ],
		default: []
	},
	dealingWith: {
		type: String,
		trim: true,
		required: [ true, "Dealing with text is required!" ]
	}
});

const Disorder = mongoose.model("disorder", DisorderSchema);

module.exports = Disorder;