const express = require("express");
const app = express();

// Authors info
const authorInfo = require("./config/author.info.json");

// Imported routers
const userRoutes = require("./routes/user.route");
const articleRoutes = require("./routes/article.route");
const tipsRoutes = require("./routes/tip.route");
const roadMapRoutes = require("./routes/roadMap.route");
const answerRoutes = require("./routes/answer.route");
const questionRoutes = require("./routes/question.route");
const disorderRoutes = require("./routes/disorder.route");

const bodyParser = require("body-parser");
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }));

app.get("/api/info", (_, res) =>
	res.status(200).send(authorInfo)
);


// Authentication routes
app.use("/api", userRoutes);

// Article CRUD routes
app.use("/api", articleRoutes);

// Answer CRUD routes
app.use("/api", answerRoutes);

// Question CRUD routes
app.use("/api", questionRoutes);

// Tips CRUD routes
app.use("/api", tipsRoutes);

// RoadMaps CRUD routes
app.use("/api", roadMapRoutes);

// Disorder CRUD routes
app.use("/api", disorderRoutes);

module.exports = app;
