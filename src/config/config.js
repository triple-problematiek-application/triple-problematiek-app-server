require("dotenv").config();

module.exports = {
	dateFormat: {
		weekday: "long",
		year: "numeric",
		month: "long",
		day: "numeric",
		hour: "numeric",
		minute: "numeric",
		second: "numeric",
		hour12: false
	},
	locale: undefined,
	logger: require("tracer").console({
		level: process.env.LOG_LEVEL || "trace",
		format: [
			"{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})",
			{
				error:
					"{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})\nCall Stack:\n{{stack}}" // error format
			}
		],
		dateformat: "HH:MM:ss.L"
	}),
	security: {
		bcrypt: {
			saltRounds: process.env.SALT_ROUNDS || 12
		},
		jwt: {
			accessToken: process.env.ACCESS_TOKEN_SECRET
		}
	},
	errorDate: function() {
		return new Date().toLocaleString(this.locale, this.dateFormat);
	}
};
