const jwt = require("jsonwebtoken");
const config = require("../config/config");

const logger = config.logger;
const title = "Middleware function:";

/**
 * Authenticate incoming requests from users or administrators
 * * Adds a property request.user to the request, this property hold the payload of the given token
 * @param { "Bearer [token]" } req The request with the used Authentication header, here is [token] the token.
 * @param { id, emailAddress, administrator } res The response with the given object added to the request (req.user) if the middleware succeeds
 * @param next Next is called when the middleware succeeds, Express continues to the next middleware or the route
 */
module.exports = (req, res, next) => {
	logger.info(title, "Authentication");
	const authHeader = req.headers.authorization;
	const token = authHeader && authHeader.split(" ")[1];
	if (token == null) return res.status(401).send({ error: "User not authenticated!", date: config.errorDate() });

	jwt.verify(token, config.security.jwt.accessToken, (err, user) => {
		if (err) return res.status(401).send({ error: "User not authenticated!", date: config.errorDate() });
		req.user = user;
		next();
	});
};
