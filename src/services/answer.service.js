const Answer = require ("../models/question/question.model");
const config = require("../config/config");

const logger = config.logger;
const title = "Answer service function";

const service = {};

// eslint-disable-next-line no-unused-vars
service.createAnswer = ({ owner, date, ...answerProperties }, user) => {
	logger.info(title, "Create answer");
	answerProperties.owner = user.id;
	return Answer.create(answerProperties);
};

// eslint-disable-next-line no-unused-vars
service.updateAnswer = (questionID, answerID, user, { owner, date, ...answerProperties }) => {
	logger.info(title, "Update answer");
	const where = { _id: answerID };
	if (!user.administrator) {
		where.owner = user.id;
	}
	return Answer.findOneAndUpdate(where, answerProperties, { upsert: false, new: true, runValidators: true })
		.then(answer => {
			if (answer) return answer;
			else return Promise.reject("There is no answer with this _id, or you do not have the rights to change it!");
		});
};

// eslint-disable-next-line no-unused-vars
service.deleteAnswer = (questionID, answerID, user) => {
	logger.info(title, "Delete answer");
	const where = { _id: answerID };
	if (!user.administrator) {
		where.owner = user.id;
	}
	return Answer.findOneAndDelete(where)
		.then(answer => {
			if (answer) return answer;
			else return Promise.reject("There is no answer with this _id, or you do not have the rights to delete it!");
		});
};

module.exports = service;