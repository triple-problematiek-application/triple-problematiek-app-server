const RoadMap = require("../models/roadMap.model");
const config = require("../config/config");

const logger = config.logger;
const title = "RoadMap service function:";

const service = {};

/**
 * This function is used to create a new RoadMap in the database
 * @param { title, ?cover, description, [ steps ], } roadMapProperties The properties of the to be added roadMap
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, title, ?cover, description, [ steps ], owner, date } a valid RoadMap object from the database
 */
// eslint-disable-next-line no-unused-vars
service.createRoadMap = ({ date, ...roadMapProperties }, user) => {
	logger.info(title, "Create RoadMap");
	roadMapProperties.owner = user.id;
	return RoadMap.create(roadMapProperties);
};

/**
 * This function is used to update an existing RoadMap in the database
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @param { ?title, ?description, ?cover, ?steps } roadMapProperties The properties of the to be updated roadMap
 * @returns { title, description, cover, steps, owner, date } a valid RoadMap object which got deleted from the database
 */
// eslint-disable-next-line no-unused-vars
service.updateRoadMap = (roadMapId, user, { owner, date, ...roadMapProperties }) => {
	logger.info(title, "Update roadMap");
	const where = user.administrator ? { _id: roadMapId } : { _id: roadMapId, owner: user.id };
	return RoadMap.findOneAndUpdate(where, roadMapProperties, { upsert: false, new: true,  runValidators: true })
		.then(article => {
			if (article) return article;
			else return Promise.reject("There is no roadMap with this _id, or you do not have the rights to change it!");
		});
};

/**
 * This function is used to get an array of RoadMap objects
 * @param titleQuery optional string that filters the roadMaps that start with the given string
 * @returns { _id, title, description, ?cover, steps, owner, date } an array of valid RoadMap objects from the database
 */
service.getRoadMaps = titleQuery => {
	logger.info(title, "Get many roadMaps");
	const filter = titleQuery == undefined ? {} : { title: new RegExp(titleQuery, "i") };
	return RoadMap.find(filter).populate("owner");
};

/**
 * This function is used to delete an existing RoadMap from the database
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { title, body, cover, positiveBulletPoints, negativeBulletPoints, owner, date } a valid RoadMap object which got deleted from the database
 */
service.deleteRoadMap = (roadMapId, user) => {
	logger.info(title, "Delete roadMap");
	const where = user.administrator ? { _id: roadMapId } : { _id: roadMapId, owner: user.id };
	return RoadMap.findOneAndDelete(where)
		.then(roadMap => {
			if (roadMap) return roadMap;
			else return Promise.reject("There is no roadMap with this _id, or you do not have the rights to change it!");
		});
};

/**
 * This function is used to get the details of an roadMap
 * @param { _id, title, description, ?cover, steps, owner, date } roadMapId valid RoadMap object from the database
 */
service.getRoadMap = roadMapId => {
	logger.info(title, "Get one roadMap");
	return RoadMap.findById(roadMapId).populate("owner").then(roadMap => {
		if (roadMap) return roadMap;
		else return Promise.reject("There is no roadMap with this _id!");
	});
};

/**
 * This function is used to get all the roadMaps the currently logged in user created
 * @param titleQuery optional string that filters the roadMaps that start with the given string
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least the administrator property
 * @returns { _id, title, body, category, ?cover, owner, date, verified } an array of valid RoadMap objects from the database
 */
service.myRoadMaps = (user, titleQuery) => {
	logger.info(title, "Get own roadMaps");
	const filter = titleQuery == undefined ? { owner: user.id } : { title: new RegExp(titleQuery, "i"), owner: user.id };
	return RoadMap.find(filter).populate("owner");
};

module.exports = service;
