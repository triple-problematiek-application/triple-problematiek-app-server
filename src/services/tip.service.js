const Tip = require("../models/tip.model");
const config = require("../config/config");

const logger = config.logger;
const title = "Tip service function:";

const service = {};

/**
 * This function is used to create a new Tip in the database
 * @param { title, ?cover, description, [ positiveBulletPoints ], [ negativeBulletPoints ] } tipProperties The properties of the to be added tip
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, title, ?cover, description, [ positiveBulletPoints ], [ negativeBulletPoints ], owner, date } a valid Tip object from the database
 */
// eslint-disable-next-line no-unused-vars
service.createTip = ({ date, ...tipProperties }, user) => {
	logger.info(title, "Create Tip");
	tipProperties.owner = user.id;
	return Tip.create(tipProperties);
};

/**
 * This function is used to update an existing Tip in the database
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @param { ?title, ?body, ?cover, ?positiveBulletPoints, ?negativeBulletPoints } tipProperties The properties of the to be updated tip
 * @returns { title, body, cover, positiveBulletPoints, negativeBulletPoints, owner, date } a valid Tip object which got deleted from the database
 */
// eslint-disable-next-line no-unused-vars
service.updateTip = (tipId, user, { owner, date, ...tipProperties }) => {
	logger.info(title, "Update tip");
	const where = user.administrator ? { _id: tipId } : { _id: tipId, owner: user.id };
	return Tip.findOneAndUpdate(where, tipProperties, { upsert: false, new: true,  runValidators: true })
		.then(article => {
			if (article) return article;
			else return Promise.reject("There is no tip with this _id, or you do not have the rights to change it!");
		});
};

/**
 * This function is used to get an array of Tips objects
 * @param titleQuery optional string that filters the tips that start with the given string
 * @returns { _id, title, description, ?cover, positiveBulletPoints, negativeBulletPoints, owner, date } an array of valid Tip objects from the database
 */
service.getTips = titleQuery => {
	logger.info(title, "Get many tips");
	const filter = titleQuery == undefined ? {} : { title: new RegExp(titleQuery, "i") };
	return Tip.find(filter).populate("owner");
};

/**
 * This function is used to delete an existing Tip from the database
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, title, body, cover, positiveBulletPoints, negativeBulletPoints, owner, date } a valid Tip object which got deleted from the database
 */
service.deleteTip = (tipId, user) => {
	logger.info(title, "Delete tip");
	const where = user.administrator ? { _id: tipId } : { _id: tipId, owner: user.id };
	return Tip.findOneAndDelete(where)
		.then(tip => {
			if (tip) return tip;
			else return Promise.reject("There is no tip with this _id, or you do not have the rights to change it!");
		});
};

/**
 * This function is used to get the details of an tip
 * @param { _id, title, description, ?cover, positiveBulletPoints, negativeBulletPoints, owner, date } tipId a valid Tip object from the database
 */
service.getTip = tipId => {
	logger.info(title, "Get one tip");
	return Tip.findById(tipId).populate("owner").then(tip => {
		if (tip) return tip;
		else return Promise.reject("There is no tip with this _id!");
	});
};

/**
 * This function is used to get all the tips the currently logged in user created
 * @param titleQuery optional string that filters the tips that start with the given string
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least the administrator property
 * @returns { _id, title, body, category, ?cover, owner, date, verified } an array of valid Tip objects from the database
 */
service.myTips = (user, titleQuery) => {
	logger.info(title, "Get own tips");
	const filter = titleQuery == undefined ? { owner: user.id } : { title: new RegExp(titleQuery, "i"), owner: user.id };
	return Tip.find(filter).populate("owner");
};

module.exports = service;
