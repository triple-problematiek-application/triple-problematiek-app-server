const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("../config/config");

const logger = config.logger;
const title = "User service function:";

const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]{8,}$/; // * At least 8 characters, 1 letter, 1 number and 1 special character

const service = {};

/**
 * This function is used to convert the given user into an object holding the JWT token of that user, and the first and last name of the user.
 * @param { _id, firstName, lastName, emailAddress, administrator, ...otherProperties } user The user which should be tokenized into a JWT token.
 * @returns { token, firstName, lastName } where token is the tokenized user, and first and last name of the user
 * * The payload of the token contains the following properties of the user: _id, emailAddress, administrator
 */
// eslint-disable-next-line no-unused-vars
service.signToken = ({ password, ...userProperties }) => {
	logger.info(title, "Sign Token");
	const token = jwt.sign({
		id: userProperties._id,
		emailAddress: userProperties.emailAddress,
		administrator: userProperties.administrator
	}, config.security.jwt.accessToken, { expiresIn: "1w" });
	return { ...userProperties, token };
};

/**
 * This function is used to register an administrator and it returns a token for the administrator.
 * @param { firstName, lastName, emailAddress, password, ?phoneNumber, function, administrator, ?profilePicture } userProperties the properties of the administrator.
 * @returns an object conforming to the return value of the UserService#signToken function
 */
service.registerAdministrator = (userProperties) => {
	logger.info(title, "Register Administrator");
	return register(userProperties, true);
};

/**
 * This function is used to register a user and it returns a token for the user.
 * @param { firstName, lastName, emailAddress, password, ?phoneNumber, function, administrator, ?profilePicture } userProperties the properties of the user.
 * @returns an object conforming to the return value of the UserService#signToken function
 */
service.registerUser = (userProperties) => {
	logger.info(title, "Register User");
	return register(userProperties, false);
};

/**
 * This function is used to login a user and it returns a token for the user.
 * @param { emailAddress, password } credentials the credentials of the user.
 * @returns an object conforming to the return value of the UserService#signToken function
 */
service.loginUser = ({ emailAddress, password }) => {
	logger.info(title, "Login User");
	return User.findOne({ emailAddress }, {
		_id: 1,
		firstName: 1,
		lastName: 1,
		emailAddress: 1,
		administrator: 1,
		password: 1
	})
		.then((administrator) => bcrypt.compare(password, administrator.password)
			.then(result => {
				if (result) {
					return service.signToken(administrator._doc);
				} else {
					return Promise.reject("Incorrect Password!");
				}
			})
		);

};

/**
 * This function is used to update an existing User in the database
 * @param { ?firstName, ?lastName, ?emailAddress, ?phoneNumber, ?function, ?password, ?profilePicture } userProperties The properties of the to be updated user
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, firstName, lastName, emailAddress, phoneNumber, function, profilePicture } a valid User object from the database
 */
// eslint-disable-next-line no-unused-vars
service.updateUser = (user, { administrator, ...userProperties }) => {
	logger.info(title, "Update user");
	const updateObject = {};
	const keys = Object.keys(userProperties);
	const values = Object.values(userProperties);
	for (let index = 0; index < keys.length; index++) {
		const key = keys[index];
		const value = values[index];
		if (value != null) updateObject[key] = value;
	}
	return User.findOneAndUpdate({ _id: user.id }, updateObject, { upsert: false, new: true,  runValidators: true })
		.then(user => {
			if (user) return user;
			else return Promise.reject("There is no user with this _id, or you do not have the rights to change it!");
		});
};

module.exports = service;

/**
 * This function is used to register a user (both a normal user, and an administrator) and it returns a token for the user.
 * @param { firstName, lastName, emailAddress, password, ?phoneNumber, function, administrator, ?profilePicture } userProperties the properties of the user.
 * @param isAdministrator whether or not the user is an administrator
 * @returns an object conforming to the return value of the UserService#signToken function
 */
const register = ({ password, ...userProperties }, isAdministrator) => {
	if (passwordRegex.test(password)) {
		return bcrypt.hash(password, config.security.bcrypt.saltRounds)
			.then(hashedPassword => User.create({ ...userProperties, administrator: isAdministrator, password: hashedPassword }))
			.then(administrator => service.signToken(administrator._doc));
	} else {
		return Promise.reject("This password does not conform to the requirements!");
	}
};
