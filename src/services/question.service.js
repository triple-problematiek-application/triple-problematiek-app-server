const Question = require("../models/question/question.model");
const config = require("../config/config");

const logger = config.logger;
const title = "Question service function:";

const service = {};

/**
 * This function is used to create a new Tip in the database
 * @param { title, message, category, [ { message, owner, ?date } ], [ userId ], owner, ?date } questionProperties The properties of the to be added question
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, title, message, category, [ { message, owner, ?date } ], [ userId ], owner, date } a valid Question object from the database
 */
// eslint-disable-next-line no-unused-vars
service.createQuestion = ({ answers, likes, date, ...questionProperties }, user) => {
	logger.info(title, "Create Question");
	questionProperties.owner = user.id;
	return Question.create(questionProperties);
};

// eslint-disable-next-line no-unused-vars
service.updateQuestion = (questionId, user, { answers, likes, owner, date, ...questionProperties }) => {
	logger.info(title, "Update Question");
	const where = { _id: questionId };
	if (!user.administrator) {
		where.owner = user.id;
	}
	return Question.findOneAndUpdate(where, questionProperties, { upsert: false, new: true, runValidators: true })
		.then(question => {
			if (question) return question;
			else return Promise.reject("There is no question with this _id, or you do not have the rights to change it!");
		});
};

service.getQuestions = () => 
	Question.find().populate("owner");

service.deleteQuestion = (questionId, user) => {
	logger.info(title, "Delete question");
	const where = { _id: questionId };
	if (!user.administrator) {
		where.owner = user.id;
	}
	return Question.findOneAndDelete(where)
		.then(question => {
			if (question) return question;
			else return Promise.reject("There is no document with this _id, or you do not have the rights to delete it!");
		});
};
	
service.getQuestion = questionId => 
	Question.findById(questionId).populate("owner").then(question => {
		if (question) return question;
		else return Promise.reject("There is no question with this _id!");
	});

module.exports = service;
