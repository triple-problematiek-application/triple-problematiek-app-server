const Disorder = require("../models/disorder.model");
const config = require("../config/config");

const logger = config.logger;
const title = "Disorder service function:";

const service = {};

/**
 * This function is used to create a new Disorder in the database
 * @param { title, definition, description, symptoms, dealingWith } disorderProperties The properties of the to be added disorder
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, title, definition, description, symptoms, dealingWith } a valid Disorder object from the database
 */
// eslint-disable-next-line no-unused-vars
service.createDisorder = ({ date, ...disorderProperties }, user) => {
	logger.info(title, "Create Disorder");
	disorderProperties.owner = user.id;
	return Disorder.create(disorderProperties);
};

/**
 * This function is used to update an existing Disorder in the database
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @param { ?title, ?definition, ?description, ?symptoms, ?dealingWith } disorderProperties The properties of the to be updated disorder
 * @returns { _id, title, definition, description, symptoms, dealingWith } a valid Disorder object which got deleted from the database
 */
// eslint-disable-next-line no-unused-vars
service.updateDisorder = (disorderId, user, { _id, owner, date, ...disorderProperties }) => {
	logger.info(title, "Update disorder");
	return Disorder.findOneAndUpdate({ _id: disorderId }, disorderProperties, { upsert: false, new: true,  runValidators: true })
		.then(article => {
			if (article) return article;
			else return Promise.reject("There is no disorder with this _id, or you do not have the rights to change it!");
		});
};

/**
 * This function is used to get an array of Disorders objects
 * @param titleQuery optional string that filters the disorders that start with the given string
 * @returns { _id, title, definition, description, symptoms, dealingWith } an array of valid Disorder objects from the database
 */
service.getDisorders = titleQuery => {
	logger.info(title, "Get many disorders");
	const filter = titleQuery == undefined ? {} : { title: new RegExp(titleQuery, "i") };
	return Disorder.find(filter).populate("owner");
};

/**
 * This function is used to delete an existing Disorder from the database
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, title, body, cover, positiveBulletPoints, negativeBulletPoints, owner, date } a valid Disorder object which got deleted from the database
 */
service.deleteDisorder = (disorderId, user) => {
	logger.info(title, "Delete disorder");
	const where = user.administrator ? { _id: disorderId } : { _id: disorderId, owner: user.id };
	return Disorder.findOneAndDelete(where)
		.then(disorder => {
			if (disorder) return disorder;
			else return Promise.reject("There is no disorder with this _id, or you do not have the rights to change it!");
		});
};

/**
 * This function is used to get the details of an disorder
 * @param disorderId the ID of the to be gotten disorder
 * @returns { _id, title, definition, description, symptoms, dealingWith } a valid Disorder object from the database
 */
service.getDisorder = disorderId => {
	logger.info(title, "Get one disorder");
	return Disorder.findById(disorderId).populate("owner").then(disorder => {
		if (disorder) return disorder;
		else return Promise.reject("There is no disorder with this _id!");
	});
};

module.exports = service;
