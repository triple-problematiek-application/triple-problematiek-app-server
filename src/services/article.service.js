const Article = require("../models/article.model");
const User = require("../models/user.model");
const config = require("../config/config");

const logger = config.logger;
const title = "Article service function:";

const service = {};

/**
 * This function is used to create a new Article in the database
 * @param { title, body, category, ?cover } articleProperties The properties of the to be added article
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, title, body, category, ?cover, owner, date, verified } a valid Article object from the database
 */
service.createArticle = (articleProperties, user) => {
	logger.info(title, "Create Article");
	articleProperties.owner = user.id;
	return setProperties(articleProperties)
		.then( article => Article.create(article));
};

/**
 * This function is used to update an existing Article in the database
 * @param { title, body, category, ?cover } articleProperties The properties of the to be updated article
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, title, body, category, ?cover, owner, date, verified } a valid Article object from the database
 */
// eslint-disable-next-line no-unused-vars
service.updateArticle = (articleId, user, { owner, date, verified, ...articleProperties }) => {
	logger.info(title, "Update article");
	const where = { _id: articleId };
	if (!user.administrator) {
		where.owner = user.id;
	}
	return Article.findOneAndUpdate(where, articleProperties, { upsert: false, new: true,  runValidators: true })
		.then(article => {
			if (article) return article;
			else return Promise.reject("There is no article with this _id, or you do not have the rights to change it!");
		});
};

/**
 * This function is used to get an array of verified Article objects
 * @param titleQuery optional string that filters the articles with titles that start with the given string
 * @returns { _id, title, body, category, ?cover, owner, date, verified } an array of valid and verified Article objects from the database
 */
service.getArticles = titleQuery => {
	logger.info(title, "Get many articles");
	const filter = titleQuery == undefined ? { verified: true } : { title: new RegExp(titleQuery, "i"), verified: true };
	return Article.find(filter).populate("owner");
};

/**
 * This function is used to get an array of unverified Article objects
 * @param titleQuery optional string that filters the articles with titles that start with the given string
 * @returns { _id, title, body, category, ?cover, owner, date, verified } an array of valid and unverified Article objects from the database
 */
service.getUnverified = titleQuery => {
	logger.info(title, "Get unverified articles");
	const filter = titleQuery == undefined ? { verified: false } : { title: new RegExp(titleQuery, "i"), verified: false };
	return Article.find(filter).populate("owner");
};

/**
 * This function is used to delete an existing Article from the database
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least an id with the users id
 * @returns { _id, title, body, category, ?cover, owner, date, verified } a valid Article object which got deleted from the database
 */
service.deleteArticle = (articleId, user) => {
	logger.info(title, "Delete article");
	const where = { _id: articleId };
	if (!user.administrator) {
		where.owner = user.id;
	}
	return Article.findOneAndDelete(where)
		.then(article => {
			if (article) return article;
			else return Promise.reject("There is no article with this _id, or you do not have the rights to change it!");
		});
};

/**
 * This function is used to get the details of an article
 * @returns { _id, title, body, category, ?cover, owner, date, verified } a valid Article object from the database
 */
service.getArticle = articleId => {
	logger.info(title, "Get one article");
	return Article.findById(articleId).populate("owner").then(article => {
		if (article) return article;
		else return Promise.reject("There is no article with this _id!");
	});
};

/**
 * This function is used to verify an article
 * @param articleId The id of the to be verified article
 * @param { administrator } user A JWT payload as given by the Authentication middleware, needs at least the administrator property
 * @returns { _id, title, body, category, ?cover, owner, date, verified } a valid Article object which got deleted from the database
 */
service.verify = (articleId, user) => {
	logger.info(title, "Verify article");
	if (user.administrator) {
		return Article.findByIdAndUpdate(articleId, { verified: true }, { upsert: false, new: true,  runValidators: true })
			.then(article => {
				if (article) return article;
				else return Promise.reject("There is no article with this _id!");
			});
	} else return Promise.reject("You must be an administrator to verify an article!");
};

/**
 * This function is used to get all the articles the currently logged in user created
 * @param titleQuery optional string that filters the articles with titles that start with the given string
 * @param { id } user A JWT payload as given by the Authentication middleware, needs at least the administrator property
 * @returns { _id, title, body, category, ?cover, owner, date, verified } an array of valid Article objects from the database
 */
service.myArticles = (user, titleQuery) => {
	logger.info(title, "Get own articles");
	const filter = titleQuery == undefined ? { owner: user.id } : { title: new RegExp(titleQuery, "i"), owner: user.id };
	return Article.find(filter).populate("owner");
};

module.exports = service;

/**
 * This function is used to set the properties of the to be added article
 * @param { title, body, category, ?cover, owner, ?date } articleProperties The properties of the to be added article
 * @returns { title, body, category, ?cover, owner, verified } The properties of the article as it should be added, the owner is checked to exist
 */
// eslint-disable-next-line no-unused-vars
const setProperties = ({ owner, date, ...articleProperties }) =>
	User.findById(owner, { administrator: 1 })
		.then(user => {
			return new Promise((resolve, reject) => {
				if (user) {
					resolve({
						...articleProperties,
						owner,
						verified: user.administrator
					});
				} else reject("This user does not exist!");
			});
		});
