const app = require("./src/app");
const config = require("./src/config/config");
const logger = config.logger;
const mongoose = require("mongoose");
require("dotenv").config();

// Catch all for unknown endpoints
app.all("*", (_, res) =>
	res.status(404).send({
		error: "Endpoint Not Found",
		date: config.errorDate()
	}));

const port = process.env.PORT || 3050;

app.on("databaseConnected", () =>
	app.listen(port, () =>
		logger.info(`server is listening on port ${port}`)));

if (process.env.MONGODB_CONNECTION_URL) {
	mongoose
		.connect(
			process.env.MONGODB_CONNECTION_URL,
			{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }
		)
		.then(() => {
			logger.info("MongoDB connection established");

			app.emit("databaseConnected");
		})
		.catch((err) => {
			logger.warn("MongoDB connection failed");
			logger.error(err);
		});
} else 
	throw Error("Error: No mongoDB connection URL defined in the environment variable 'MONGODB_CONNECTION_URL'");
